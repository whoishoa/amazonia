<%@ include file="private/Library.jsp"%>

<% title = "Product Order"; %>

<%  
	//variables used later for this page:
	String name = ""; 
	String prodSKU = null; //stores name of product clicked on prev page.
	final int INVALID = -1;
	String SKU = null; //stores SKU of product
	int price = INVALID; //stores price in double format
	int prodID = INVALID;
	boolean checkQuantity = false;
	
	if (request.getParameter("id") != null) {
		try {
			prodSKU = request.getParameter("id");
			session.setAttribute("product_SKU", prodSKU);
		}
		catch (Exception e) {
			prodSKU = null;
		}
	}
	
	if (session.getAttribute("product_id") != null) {
		prodID = (Integer) session.getAttribute("product_id");	
	}
	
	//retreive productid from product browsing page and ask user for quantity to buy.
	//TODO: make sure next line is correct later.
	//send product_id from Product_Browsing.jsp
	
	if(session.getAttribute("product_SKU") != null){   
		prodSKU = (String) session.getAttribute("product_SKU");
		
		pstmt = conn.prepareStatement("SELECT ID, name, price FROM products "
										+ "WHERE products.SKU = ?");
		
		pstmt.setString(1, prodSKU);
		rs = pstmt.executeQuery();
		
		//store variables:
		if(rs.next()) {
			
			prodID = rs.getInt("ID");
			
			//add product id so it can be used after adding quantity
			session.setAttribute("product_id", prodID);
			
			name = rs.getString("name");
			//int categoryid = rs.getInt("categoryid");
			price = rs.getInt("price");
		}
		
	}
	
	//this will run after the user enters a quantity and submits.
	if(request.getParameter("quantity") != null)
	{
		int quantity = 0;
		try {
		quantity = Integer.parseInt(request.getParameter("quantity"));
		checkQuantity = true;
	//now check if that product was already in the user's cart. If so, add new quantity to old quantity.
		
		pstmt = conn.prepareStatement("SELECT uid, pid, quantity FROM carts "
						+ "WHERE uid = ? "
						+ "AND pid = ?");
		pstmt.setInt(1, (Integer) session.getAttribute("user_id"));
		pstmt.setInt(2, (Integer) session.getAttribute("product_id"));
		
		rs = pstmt.executeQuery();
		
		int user_id = (Integer) session.getAttribute("user_id");
        prodID = (Integer) session.getAttribute("product_id");

		//check if rs is empty, add new element
		if(rs == null || !rs.isBeforeFirst())
		{
				if (quantity > 0 && session.getAttribute("product_id") != null) //add to cart
				{
					pstmt = conn.prepareStatement("INSERT INTO carts (uid, pid, quantity, price) "
												+ "VALUES (?, ?, ?, ?);");
					
					quantity = Integer.parseInt(request.getParameter("quantity"));
					pstmt.setInt(1, user_id);
					pstmt.setInt(2, prodID);
					pstmt.setInt(3, quantity);
					pstmt.setInt(4, price);
				
					pstmt.executeUpdate();
				}
		}
		//rs was not empty, so we need to update the old cart row.
		else
		{
			if(rs.next()) //should always return true, but move forward.
			{
				int oldQuantity = rs.getInt("quantity");
				int quantityToAdd = 0;
				try {
					quantityToAdd = Integer.parseInt(request.getParameter("quantity"));
				}
				catch (Exception e) {
					quantityToAdd = 0;
				}
				int newQuantity = oldQuantity + quantityToAdd;
				pstmt = conn.prepareStatement("UPDATE Carts "
							+" SET quantity=?"
							+" WHERE uid=?"
							+" AND pid=?");
	
				pstmt.setInt(1, newQuantity);
                pstmt.setInt(2, user_id);
                pstmt.setInt(3, prodID);
				pstmt.executeUpdate();
			}
		}
		session.removeAttribute("product_id");
		session.removeAttribute("product_SKU");
		response.sendRedirect("browse");
		}
		catch (Exception e) {
			checkQuantity = false;
		}
	}
%>

<%@ include file="private/header.jsp" %>
<%@ include file="private/middle.jsp" %>

<% if (session.getAttribute("role") != null && session.getAttribute("role").equals("customer")) {%>
	<br />
<% if (!checkQuantity) { %>
	<div class="errorDialog">
		Please enter a valid quantity value.
	</div>
<% } %>
	<%
	//if a product id was sent, as was valid, allow user to enter a quantity
	if(prodSKU != null) { %>
		
		<h3> Update your cart: </h3>
		<form method="POST" action="order">
			<table border=1 cellpadding=5 align="center">
			<tr>
				<th>Product Name</th>
				<th>SKU</th>
				<th>Price</th>
				<th>Quantity</th>
			</tr>
			<tr>
				<td><%=name%></td>
				<td><%=prodSKU%></td>
				<td><%=formatPrice(price)%></td>
				<td><input type="text" name="quantity" size="6"/></td>
			</tr>
			</table>
		<input type="submit" name="add_quantity" value="Add Quantity to Cart" />
		</form>
		
	<br />
	<br />
	<br />
	<% } %>
	
	<h3>Your Shopping Cart:</h3>
	<% 
	pstmt = conn.prepareStatement("SELECT * FROM carts WHERE uid="+session.getAttribute("user_id"));
	rs = pstmt.executeQuery();
	
	if (rs.next()) {
	%>
        <table border=1 cellpadding=5 align="center">
          <tr>
            <th>Product Name</th>
            <th>SKU</th>
            <th>Quantity</th>
			<th>Price</th>
			<th>Amount Price</th>
          </tr>
		  
		  <%	//Display user's cart
		  int user_id = -1;
		  double sum=0; //for grand total
		  
		  if (session.getAttribute("user_id") == null) {
			  user_id = -1;
		  }
		  else {
			  user_id = (Integer) session.getAttribute("user_id");
			  
				pstmt = conn.prepareStatement("SELECT p.name, p.SKU, c.quantity, p.price "
												+"FROM products AS p,carts AS c "
												+"WHERE c.uid = ? "
												+"AND c.pid = p.ID");
									
					
				pstmt.setInt(1, user_id);
				rs = pstmt.executeQuery();
				
				while(rs.next()) { %>
				<tr>
					<td><%=rs.getString("name")%></td>
					<td><%=rs.getString("SKU")%></td>
					<td><%=rs.getInt("quantity")%></td>
					<td>$<%=formatPrice(rs.getInt("price"))%></td>
					<%double quantityAppliedPrice = rs.getInt("quantity") * rs.getInt("price");
					  sum += quantityAppliedPrice;%>
					<td>$<%=formatPrice(quantityAppliedPrice)%></td>
				</tr>
				<% } }%>
				
			</table>
			<br>
			Grand Total = $<%=formatPrice(sum)%>
		<% } else { %>
			<div class="errorDialog">
				Your cart is currently empty.
			</div>
		<% } %>
<% } else { /* The user is not logged in or not a customer */ %>

<div class="errorDialog">
	You must be logged in as a customer to view this page!
</div>

<% } %>		
<%@ include file="private/ender.jsp" %>
