<%
if (session.getAttribute("user_id") == null) {
	response.sendRedirect("login");
} else {
	if (session.getAttribute("role").equals("customer")) {
		response.sendRedirect("browse");
	} else {
		response.sendRedirect("products");
	}
}
%>
