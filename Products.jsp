<%@ include file="private/Library.jsp"%>

<% title = "Products"; %>

<%
	String deltt = null;
	String uptt = null;
	
	String fail = "";
	boolean added = false;
	
	//if user submitted a Product Add
	if(request.getParameter("submit") != null && (request.getParameter("submit")).equals("Add")) {
		
		String product_name = request.getParameter("prodname");
		
		if (product_name.length() == 0) {
			fail = "failed";
		}
		
		String SKU = null;
		double price = 0.0;
		
		try {
			price = Double.parseDouble(request.getParameter("prodprice"));
			if (price <= 0) {
				fail = "failed";
			}
			SKU = request.getParameter("prodSKU");
		} catch(Exception ex) {
			fail = "failed";
		}
		
		pstmt = conn.prepareStatement("SELECT sku FROM products WHERE sku = ?");
		pstmt.setString(1, SKU);
		rs = pstmt.executeQuery();
		if (rs.next()) {
			fail = "failed";
		}
		
		int category_id = Integer.parseInt(request.getParameter("catid"));
		
		if (fail.length() == 0) {
			
			//add to database
			pstmt = conn.prepareStatement("INSERT INTO products (name, sku, cid, price) "
												+ "VALUES (?, ?, ?, ?)");
												
			pstmt.setString(1, product_name);
			pstmt.setString(2, SKU);
			pstmt.setInt(3, category_id);
			pstmt.setDouble(4, price);
			pstmt.executeUpdate();
			added = true;
		}
	} else if(request.getParameter("submit") != null && (request.getParameter("submit")).equals("Delete")) {
		
		String SKU = request.getParameter("SKU");
		deltt = SKU;
		//delete
		pstmt = conn.prepareStatement("DELETE FROM products "
									+ "WHERE SKU = ?");
		pstmt.setString(1, SKU);
		pstmt.executeUpdate();
	}
	
	
	//if user submitted an update
	else if(request.getParameter("submit") != null && (request.getParameter("submit")).equals("Update")) {
		String product_name = request.getParameter("pname");
		String SKU = request.getParameter("SKU");
		String oldsku = request.getParameter("skuu");
		uptt = oldsku;
		double price = Double.parseDouble(request.getParameter("pprice"));
		int category_id = Integer.parseInt(request.getParameter("cname"));
		
		pstmt = conn.prepareStatement("UPDATE products "
									+ "SET name=?, sku=?, cid=?, price=? "
									+ "WHERE sku = ?");
		pstmt.setString(1, product_name);
		pstmt.setString(2, SKU);
		pstmt.setInt(3, category_id);
		pstmt.setDouble(4, price);
		pstmt.setString(5, oldsku);
		
		pstmt.executeUpdate();
	}
%>

<%@ include file="private/header.jsp"%>

<script>
	<%pstmt = conn.prepareStatement("SELECT  cat.name AS catname, p.name AS prodname, cat.ID AS cid, p.SKU, p.price "
			+"FROM products AS p "
			+"JOIN categories AS cat "
			+"ON p.cid = cat.id "
			+"ORDER BY cat.ID");
												
	rs = pstmt.executeQuery(); 
	%>
	
catProds = [
	<%
	ArrayList catNames = new ArrayList<String>();
	String currCatName = "";
	String prevCatName = "";
	boolean firstRun = true;
	int i = 0;
	boolean start = rs.next();
	while(start) { 
		prevCatName = currCatName;
		currCatName = rs.getString("catname");
		catNames.add(currCatName);
	%>
	
	<% if (i != 0) { out.print(","); } %> {
		"category": "<%= currCatName %>",
		"products": [
			{
				"name":"<%= rs.getString("prodname") %>",
				"SKU":"<%= rs.getString("SKU") %>",
			    "price":"<%= rs.getDouble("price")  %>",
				"cid":"<%= rs.getInt("cid") %>"
			} 
			<% 
			start = rs.next();
			while (start) {
				prevCatName = currCatName;
				currCatName = rs.getString("catname");
				if (!prevCatName.equals(currCatName)) {
					break; 
				} else {
			%>
			, { 
				"name":"<%= rs.getString("prodname") %>",
				"SKU":"<%= rs.getString("SKU") %>",
			    "price":"<%= rs.getDouble("price")  %>",
				"cid":"<%= rs.getInt("cid") %>"
			}
			<% 
				} 
				start = rs.next();
			}%>
		
		]
	}
	<%
		i ++;
	} 
	%>
	];



function pchainj(ii) { 
	var notCatProdsSize = catProds.length + 1;
	if (ii < notCatProdsSize) {
		if (catProds[ii].products.length >0) {
		document.write("<table border=1 width='100%' style='margin-left:-42px;'>");
		
		document.write("<tr><td width='50%'>Name</td><td width='15%'>SKU</td><td width='15%'>Price</td><td width='10%'>Category</td><td width='10%'>Edit</td></tr>")
	
		for (var i = 0; i < catProds[ii].products.length; i ++) {
			document.write("<form action='products' method='POST'>");
			document.write("<tr><td><textarea name='pname'>"+catProds[ii].products[i].name+"</textarea></td>");
			document.write("<td><textarea name='SKU'>"+catProds[ii].products[i].SKU+"</textarea><input type='hidden' name='skuu' value='" + catProds[ii].products[i].SKU + "'></td>");
			document.write("<td><textarea name='pprice'>"+catProds[ii].products[i].price+"</textarea></td>");
			document.write("<td><select id='c" + ii + "x" + i + "' name='cname'>" + $("#selectf").html() + "</select></td>");
			document.write("<td>"); 
	
			/*the delete button is here:  */
			document.write("<input type='submit' name='submit' value='Delete'><br>");
	
			/*The update button is here: */
			document.write("<input type='submit' name='submit' value='Update'><br>");
	
			document.write("</td></tr></form>");
			$('#c'+ ii + "x" + i).val( catProds[ii].products[i].cid );
		}
	
		document.write("</table>");
		}  
	}
	else {
		if (catProds.length >0) {
			document.write("<table border=1 width='100%' style='margin-left:-42px;'>");
			
			document.write("<tr><td width='50%'>Name</td><td width='15%'>SKU</td><td width='15%'>Price</td><td width='10%'>Category</td><td width='10%'>Edit</td></tr>")
		
			for (var i = 0; i < catProds.length; i ++) {
				for (var j = 0; j < catProds[i].products.length; j++) {
					document.write("<form action='products' method='POST'>");
					document.write("<tr><td><textarea name='pname'>"+catProds[i].products[j].name+"</textarea></td>");
					document.write("<td><textarea name='SKU'>"+catProds[i].products[j].SKU+"</textarea><input type='hidden' name='skuu' value='" + catProds[i].products[j].SKU + "'></td>");
					document.write("<td><textarea name='pprice'>"+catProds[i].products[j].price+"</textarea></td>");
					document.write("<td><select id='c" + i + "x" + j + "' name='cname'>" + $("#selectf").html() + "</select></td>");
					document.write("<td>"); 
			
					/*the delete button is here:  */
					document.write("<input type='submit' name='submit' value='Delete'><br>");
			
					/*The update button is here: */
					document.write("<input type='submit' name='submit' value='Update'><br>");
			
					document.write("</td></tr></form>");
					$('#c'+ i + "x" + j).val( catProds[i].products[j].cid );
				}
			}
		
			document.write("</table>");
			}  
	}
};

function cchange(ii) { 
	var sfield = $("#searchfield" + ii);
	var ta = "";
	var notCatProdsSize = catProds.length + 1;
	if (ii < notCatProdsSize) {
		if (catProds[ii].products.length >0) {
			ta += "<table border=1 width='100%' style='margin-left:-42px;'>";
			
			ta += "<tr><td width='50%'>Name</td><td width='15%'>SKU</td><td width='15%'>Price</td><td width='10%'>Category</td><td width='10%'>Edit</td></tr>";
	
			var tabdel = $("#tabdel" + ii);
			tabdel.empty();
			
			for (var i = 0; i < catProds[ii].products.length; i ++) {
				if (catProds[ii].products[i].name.indexOf(sfield.val()) != -1) {
					ta +="<form action='products' method='POST'>";
					ta +="<tr><td><textarea name='pname'>"+catProds[ii].products[i].name+"</textarea></td>";
					ta +="<td><textarea name='SKU'>"+catProds[ii].products[i].SKU+"</textarea><input type='hidden' name='skuu' value='" + catProds[ii].products[i].SKU + "'></td>";
					ta +="<td><textarea name='pprice'>"+catProds[ii].products[i].price+"</textarea></td>";
					ta +="<td><select id='c" + ii + "x" + i + "' name='cname'>" + $("#selectf").html() + "</select></td>";
					ta +="<td>"; 
	
					/*the delete button is here:  */
					ta +="<input type='submit' name='submit' value='Delete'><br>";
	
					/*The update button is here: */
					ta +="<input type='submit' name='submit' value='Update'><br>";
	
					ta +="</td></tr></form>";
					$('#c'+ ii + "x" + i).val( catProds[ii].products[i].cid );
				}
			}
	
			ta +="</table>";
		} 
	}
	else {
		if (catProds.length >0) {
			ta += "<table border=1 width='100%' style='margin-left:-42px;'>";
			
			ta += "<tr><td width='50%'>Name</td><td width='15%'>SKU</td><td width='15%'>Price</td><td width='10%'>Category</td><td width='10%'>Edit</td></tr>";
	
			var tabdel = $("#tabdel" + ii);
			tabdel.empty();
			
			for (var i = 0; i < catProds.length; i ++) {
				for (var j = 0; j < catProds[i].products.length; j++) {
					if (catProds[i].products[j].name.indexOf(sfield.val()) != -1) {
						ta +="<form action='products' method='POST'>";
						ta +="<tr><td><textarea name='pname'>"+catProds[i].products[j].name+"</textarea></td>";
						ta +="<td><textarea name='SKU'>"+catProds[i].products[j].SKU+"</textarea><input type='hidden' name='skuu' value='" + catProds[i].products[j].SKU + "'></td>";
						ta +="<td><textarea name='pprice'>"+catProds[i].products[j].price+"</textarea></td>";
						ta +="<td><select id='c" + i + "x" + j + "' name='cname'>" + $("#selectf").html() + "</select></td>";
						ta +="<td>"; 
		
						/*the delete button is here:  */
						ta +="<input type='submit' name='submit' value='Delete'><br>";
		
						/*The update button is here: */
						ta +="<input type='submit' name='submit' value='Update'><br>";
		
						ta +="</td></tr></form>";
						$('#c'+ i + "x" + j).val( catProds[i].products[j].cid );
					}
				}
			}
	
			ta +="</table>";
		}
	}
	
	tabdel.html(ta);

};

$(function() {
    $( "#tabs" ).tabs();
});

$(function() {
    $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
    $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
});
</script>

<style>


.ui-tabs-vertical .ui-tabs-nav {
	padding: .2em .1em .2em .2em;
	float: left;
	width: 12em;
}

.ui-tabs-vertical .ui-tabs-nav li {
	clear: left;
	width: 100%;
	border-bottom-width: 1px !important;
	border-right-width: 0 !important;
	margin: 0 -1px .2em 0;
}

.ui-tabs-vertical .ui-tabs-nav li a {
	display: block;
}

.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active {
	padding-bottom: 0;
	padding-right: .1em;
	border-right-width: 1px;
	border-right-width: 1px;
}

.ui-tabs-panel {
	margin: 0 auto;
	width: 600px;
	position: relative;
}

.wrapper {
	margin: 0 auto;
	width: 80%;
}

</style>

<%@ include file="private/middle.jsp"%>

<% if (session.getAttribute("role") != null && session.getAttribute("role").equals("owner")) { %>

<% if ( session.getAttribute("loginstatus").equals("justsignedin") ) { 
		session.setAttribute("loginstatus", "loggedin");
	%>
<div class="successDialog">
  You have successfully signed into
  <%= session.getAttribute("name") %>!
</div>
<% } else if ( session.getAttribute("loginstatus").equals("justregistered") ) { 
		session.setAttribute("loginstatus", "loggedin");
	%>
<div class="successDialog">
  You have successfully registed the user
  <%= session.getAttribute("name") %>!
</div>
<% } %>

<% if (deltt != null) { %>
<div class="successDialog">
  Deleted product with SKU of
  <%= deltt %>!
</div>
<% } %>

<% if (uptt != null) { %>
<div class="successDialog">
  Updated product with previous SKU of
  <%= uptt %>!
</div>
<% } %>

<%
	if (fail.length() > 0) {
	%>
<div class="errorDialog">Failure to insert new product</div>
<% } else if (added) { %>

<div class="successDialog">
  The following product has been successfully added:<br>
  <br> Name:
  <%= request.getParameter("prodname") %><br> SKU:
  <%= request.getParameter("prodSKU") %><br> Price:
  <%= request.getParameter("prodprice") %><br> Category:
  <%
		pstmt = conn.prepareStatement("SELECT name FROM categories WHERE ID = ?");
		pstmt.setInt(1, Integer.parseInt(request.getParameter("catid")));
		rs = pstmt.executeQuery();

		rs.next();
		out.println(rs.getString("name"));
		%>
</div>

<% } %>



<h1>Products</h1>
<div class="wrapper">
	<div id="tabs">
	
	  <h2>Add a new product</h2>
	  <form action="products" method="POST">
	    Name: <input type="text" name="prodname"><br>
	    <br> SKU: <input type="text" name="prodSKU"><br>
	    <br> Price: <input type="text" name="prodprice"><br>
	    <br> Category: <select name="catid" id="selectf"
	      onchange="chainj()">
	      <%
						//get the categories list into rs
						pstmt = conn.prepareStatement("SELECT ID, name FROM categories ORDER BY ID ");
						rs = pstmt.executeQuery();
					%>
	      <% while(rs.next()) { %>
	      <option value=<%= rs.getInt("ID")%>>
	        <%=rs.getString("name")%>
	      </option>
	      <% } //end while %>
	    </select> <br>
	    <br> <input type="submit" name="submit" value="Add">
	
	  </form>
	  <br>
	  <br>
	
	  <hr
	    style="border-top: 1px solid; border-bottom: 1px solid; height: 5px; border-left: 0px; border-right: 0px;">
	  <br>
	
	  <ul>
	    <% for (int j = 1; j <= catNames.size(); j++) { %>
	    <li><a href="#tabs-<%= j %>"><%= catNames.get(j - 1) %></a></li>
	    <% } %>
	  </ul>
	
	  <% for (int j = 1; j <= catNames.size(); j++) { %>
	  <div id="tabs-<%= j %>" class="element">
	    <h2>Current Products</h2>
	    Search Product: <input type="search" id="searchfield<%= j - 1 %>"
	      name="prodsearch">
	    <button value="Search" onclick="cchange(<%= j - 1 %>)">Search</button>
	    <br>
	    <br>
	    
	    <div id='tabdel<%= j-1 %>'>
	      <script>pchainj(<%= j-1 %>)</script>
	    </div>
	  </div>
	  <% } %>
	
	</div>
</div>

<% } else { %>

<div class="errorDialog">You must be logged in as an owner to view
  this page!</div>

<% } %>

<%@ include file="private/ender.jsp"%>