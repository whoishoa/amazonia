<%@tag description="The Main Structure For Each Page" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<%@attribute name="title" fragment="true" %>
<%@attribute name="js" fragment="true" %>
<%@attribute name="css" fragment="true" %>
<%@attribute name="inlinecss" fragment="true" %>
<%@attribute name="inlinejs" fragment="true" %>
<%@attribute name="startjs" fragment="true" %>

<t:header>
  <jsp:attribute name="title">
    <jsp:invoke fragment="title"/>
  </jsp:attribute>
  <jsp:attribute name="js">
    <jsp:invoke fragment="js"/>
  </jsp:attribute>
  <jsp:attribute name="css">
    <jsp:invoke fragment="css"/>
  </jsp:attribute>
  <jsp:attribute name="inlinecss">
    <jsp:invoke fragment="inlinecss"/>
  </jsp:attribute>
  <jsp:attribute name="inlinejs">
    <jsp:invoke fragment="inlinejs"/>
  </jsp:attribute>
  <jsp:attribute name="startjs">
    <jsp:invoke fragment="startjs"/>
  </jsp:attribute>
</t:header>

<div id="contentdiv">
  <jsp:doBody/>
</div>
	
<t:ender>

</t:ender>