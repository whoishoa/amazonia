<%@tag description="The Main Header For Each Page" pageEncoding="UTF-8"%>

<%@attribute name="title" fragment="true" %>
<%@attribute name="js" fragment="true" %>
<%@attribute name="css" fragment="true" %>
<%@attribute name="inlinecss" fragment="true" %>
<%@attribute name="inlinejs" fragment="true" %>
<%@attribute name="startjs" fragment="true" %>

<!DOCTYPE html>
<html>
  <head>
    <title><jsp:invoke fragment="title" /></title>
    <!-- <base href="http://myoneclass.com/" target="_blank"> -->
    <link rel="icon" type="image/png" href="imgs/favicon.png">
	
	<!-- CSS IMPORTS -->
    <link rel="stylesheet" type="text/css" href="css/main.css" />
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    
	<jsp:invoke fragment="css" />
	
	<!-- INLINE CSS -->
	<style>
	  <jsp:invoke fragment="inlinecss" />
	</style>
	
	<!-- JS IMPORTS -->
	<script src="js/library.js"></script>
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<jsp:invoke fragment="js" />
	
	<!-- INLINE JS -->
	<script>
	  window.onload = start;
	  
	  function start() {
	    
	  
	    <jsp:invoke fragment="startjs" />
	  };
	  
	  <jsp:invoke fragment="inlinejs" />
	</script>
	
  </head>
  <body>
    <div id="headerdiv" class="clickable" onclick="window.location='http://myoneclass.com/'">
      <img src="imgs/title.png">
    </div>