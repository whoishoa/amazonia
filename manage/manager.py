"""
File: manager.py
Author: Hoa Mai
Date: 04/26/2014

Description: 
====================
The purpose of this program is to provide you with a easy
   and stressless way to manage the database and server.
   Make sure your running this file as the postgres user!
   
How to run this file:
====================
> python
>>> from manager import *
>>> *function you want to run*

e.g:
> python
>>> from manager import *
>>> update()
   
Methods:
====================
String updateGit() - does a git pull to update all of the files
String restartApache() - restarts Tomcat7
String restartDatabase() - restarts the database server
String updateAll() - runs updateGit(), restartApache(), and restartDatabase()

Common Directories:
====================
### TOMCAT7 ###
Apache Config Directory: /var/lib/tomcat7/conf/
Another Import Apache Config file: /etc/default/tomcat7
Website Root: /var/lib/tomcat7/webapps/ROOT/

### POSTGRES ###
Database files location Directory: /var/lib/postgresql/9.1/main
Postgres commands Directory:/usr/lib/postgresql/9.1/bin
Config Files Directory: /etc/postgresql/9.1/main

SOFTWARE DEPENDENCY:
====================
tomcat7
postgres 9.1
git
"""

# Imports
import pexpect

GIT = {
    "entranceKey" : '^\x8a\x82\x89\x86\x8c\x8f~\x91\x82OP>'
}

# Updates all the files to the latest git HEAD
def updateGit():
    responses = [
        "Password for 'https://whoishoa@bitbucket.org': ", 
        "fatal:"]
    child = pexpect.spawn("/bin/bash -c 'cd /var/lib/tomcat7/webapps/ROOT/ && sudo git pull origin master'")
    resp = child.expect(responses)
    if resp == 0:
        child.sendline(rc(GIT["entranceKey"]));
    ret = "> git pull origin master";
    nextline = child.readline()
    while nextline != '':
        nextline = child.readline()
    return ret
    
# This can only be called manually in a python shell as root
# Apache Server CONFIG: /var/lib/tomcat7/conf/
# Website Root: /var/lib/tomcat7/webapps/ROOT/
def restartApache():
    return pexpect.run("/bin/bash -c 'sudo /etc/init.d/tomcat7 restart'")

# This function restarts the database server. You must be logged in as postgres
# Database files location Directory: /var/lib/postgresql/9.1/main
# Postgres commands Directory:/usr/lib/postgresql/9.1/bin
# Config Files Directory: /etc/postgresql/9.1/main
def restartDatabase():
    return pexpect.run("/bin/bash -c '/usr/lib/postgresql/9.1/bin/pg_ctl -m fast -D /var/lib/postgresql/9.1/main restart'")

# Just does a git pull and a apache restart.
def update():
    print updateGit()
    print "> /etc/init.d/tomcat7 restart"
    print restartApache()

# This can only be called manually in a python shell
# Updates all of the files by doing a git pull, then restarts the Apache and Database servers.
# You must be logged in as postgres!
def completeUpdate():
    update()
    print "> /usr/lib/postgresql/9.1/bin/pg_ctl -m fast -D /var/lib/postgresql/9.1/main restart"
    print restartDatabase()
    
def rc(s):
    n = ""
    for i in range(len(s)):
        n = n + chr(ord(s[i]) - 29)
    return n
    
