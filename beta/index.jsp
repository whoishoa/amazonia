<%@ page import="java.sql.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>

<%
String DB_LOC = 
      "jdbc:postgresql://www.myoneclass.com/beta?user=postgres&password=postgres";
String pageTitle = request.getRequestURL().substring(request.getRequestURL().lastIndexOf("/") + 1);
if (pageTitle.equals("")) {
	pageTitle = "index";
}

Cookie[] cookies = request.getCookies();
Cookie user = null;

if (cookies != null) {
	for (int i = 0; i < cookies.length; i++) {
		if (cookies[i].getName().equals("user")) {
		   user = cookies[i];
		}
	}
}

boolean newUser = false;
if (user == null) {
	newUser = true;
	user = new Cookie("user", UUID.randomUUID().toString() );
	response.addCookie(user);
	
}

Connection conn = null;
PreparedStatement pstmt = null;
ResultSet rs = null;

Class.forName("org.postgresql.Driver");
conn = DriverManager.getConnection(DB_LOC);

pstmt = conn.prepareStatement("SELECT id FROM boards WHERE title = ?");
pstmt.setString(1,pageTitle);
rs = pstmt.executeQuery();

boolean firstTime = false;

if (!rs.next()) {
	pstmt = conn.prepareStatement("INSERT INTO boards (title) VALUES (?)");
	pstmt.setString(1,pageTitle);
	pstmt.execute();
	firstTime = true;
	
	pstmt = conn.prepareStatement("SELECT id FROM boards WHERE title = ?");
	pstmt.setString(1,pageTitle);
	rs = pstmt.executeQuery();
	rs.next();
} 

int boardId = rs.getInt("id");

rs.close();

pstmt = conn.prepareStatement("SELECT * FROM comments WHERE comments.ID IN (SELECT comments.id FROM comments, boards WHERE board = boards.id AND title = ? ORDER BY comments.id DESC) ORDER BY id ASC");
pstmt.setString(1,pageTitle);

ResultSet comments = pstmt.executeQuery();


%>


<!DOCTYPE html>
<html style="height:100%;">
 <head>
  <title>Discussion Board</title>
  <base href="http://www.myoneclass.com/">
  <link rel="icon" type="image/png" href="imgs/favicon.png">
  <link rel="stylesheet" type="text/css" href="css/main.css?a" />
  <link rel="stylesheet" type="text/css" href="css/common.css" />
  <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    
	
	<!-- JS IMPORTS -->
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script src="js/pointtip.js"></script>
	<script src="js/library.js"></script>
	
<style>

.postDialog {
	color:#555;
	padding:10px;
	width:750px;
	margin-right:auto;
	margin-left:auto;
	margin-top:10px;
	border:1px solid #BBB;
}

.postDialog:hover {
	cursor:pointer;
	background-color:#00AAFF;
	color:white;
	border:1px solid #00AAFF;
	
}

.postDialog::selection {
	background:black;
	color:white;
}

</style>

<script>

function start() {
	document.getElementById("commentbox").scrollTop = 10000000;
}

window.onload = start;

</script>

 </head>
 <body style="background-color:white;height:100%;">
  <div style="height:100%;width:100%;background-color:white;display:table;">
	<div style="display:table-row;">
	  <div id="commentbox" style="height:100%;overflow-y:scroll;padding-bottom:150px;">
	  
	    <% if (firstTime) { %>
		<div class="successDialog" style="padding-top:10px;">
			Congratulations! You are the first person to have ever visited this page! There are no comments yet. Why don't you post the first comment ever?
		</div>
	    <% } %>
		
		<% if (newUser) { %>
		<div class="successDialog" style="padding-top:10px;">
			Welcome to this website for the first time!
		</div>
	    <% } %>
		
		<div class="postDialog">
		<%= pageTitle %><%= user.getValue() %>
		</div>
		
		<% while (comments.next()) { %>
		<div class="postDialog">
		<%= comments.getString("comment").replace("\n", "<br />") %>
		</div>
		
		<% } %>
		
	  </div>
	</div>
	<div style="display:table-row;height:100px;">
	  <div style="display:table-cell;border-top:1px solid #BBB;text-align:center;padding:10px;padding-left:0px;padding-right:0px;">
 
      <form method="POST" action="beta/actions.jsp">
 	   <div style="width:1000px;margin-right:auto;margin-left:auto;display:table">
	    <div style="display:table-row">
		 <div style="display:table-cell;width:125px;">
		 
		 </div>
	     <div style="display:table-cell;width:750px;">
		  <input type="hidden" name="action" value="add">
		  <input type="hidden" name="board" value="<%= boardId %>">
		  <textarea id="comment" name="comment" style="height:100px;width:750px;padding:10px;outline-width:0;resize:none;" autofocus></textarea>
		 </div>
		 <div style="display:table-cell;width:125px;vertical-align:top;">
		  <input type="submit" class="mainButton" style="height:35px;width:100px;border:1px solid #00AAFF;" value="POST">
		 </div>
		</div>
	   </div>
	  </form>
		
      </div>
	</div>
	
  </div>

 </body>
</html>