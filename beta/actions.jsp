<%@ page import="java.sql.*,java.util.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>

<%
String DB_LOC = 
      "jdbc:postgresql://www.myoneclass.com/beta?user=postgres&password=postgres";
String pageTitle = request.getRequestURL().substring(request.getRequestURL().lastIndexOf("/") + 1);
if (pageTitle.equals("")) {
	pageTitle = "index";
}

Cookie[] cookies = request.getCookies();

Connection conn = null;
PreparedStatement pstmt = null;
ResultSet rs = null;

Class.forName("org.postgresql.Driver");
conn = DriverManager.getConnection(DB_LOC);

if ( "POST".equalsIgnoreCase(request.getMethod()) ) {
	String action = request.getParameter("action");
	if (action.equals("add")) {
		
		String comment = request.getParameter("comment");
		if (!comment.trim().equals("")) {
			int boardId = Integer.parseInt(request.getParameter("board"));
			
			Cookie user = null;
			
			if (cookies != null) {
				for (int i = 0; i < cookies.length; i++) {
					if (cookies[i].getName().equals("user")) {
						user = cookies[i];
					}
				}
			}
			if (user == null) {
				user = new Cookie("user", UUID.randomUUID().toString() );
				response.addCookie(user);	
			}
			
			pstmt = conn.prepareStatement("INSERT INTO comments (comment, poster, board) VALUES (?, ?, ?)");
			pstmt.setString(1,comment);
			pstmt.setString(2,user.getValue());
			pstmt.setInt(3,boardId);
			pstmt.execute();
		}
		
		
		response.sendRedirect("http://www.myoneclass.com/beta/");
	}
} else {
    out.print("ERROR");
	// response.sendRedirect("http://www.myoneclass.com/beta/");
}
%>
