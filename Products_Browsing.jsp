<%@ include file="private/Library.jsp"%>

<% title = "Browse Products"; %>
<%
	String deltt = null;
	String uptt = null;
	
	String fail = "";
	boolean added = false;
	
%>

<%@ include file="private/header.jsp"%>

<script>
	<%pstmt = conn.prepareStatement("SELECT  cat.name AS catname, p.name AS prodname, cat.ID AS cid, p.SKU, p.price "
												+"FROM products AS p "
												+"JOIN categories AS cat "
												+"ON p.cid = cat.id "
												+"ORDER BY cat.ID");
												
	rs = pstmt.executeQuery(); 
	%>
	
catProds = [
	<%
	ArrayList catNames = new ArrayList<String>();
	String currCatName = "";
	String prevCatName = "";
	boolean firstRun = true;
	int i = 0;
	boolean start = rs.next();
	while(start) { 
		prevCatName = currCatName;
		currCatName = rs.getString("catname");
		catNames.add(currCatName);
	%>
	
	<% if (i != 0) { out.print(","); } %> {
		"category": "<%= currCatName %>",
		"products": [
			{
				"name":"<%= rs.getString("prodname") %>",
				"SKU":"<%= rs.getString("SKU") %>",
			    "price":"<%= rs.getDouble("price")  %>",
				"cid":"<%= rs.getInt("cid") %>"
			} 
			<% 
			start = rs.next();
			while (start) {
				prevCatName = currCatName;
				currCatName = rs.getString("catname");
				if (!prevCatName.equals(currCatName)) {
					break; 
				} else {
			%>
			, { 
				"name":"<%= rs.getString("prodname") %>",
				"SKU":"<%= rs.getString("SKU") %>",
			    "price":"<%= rs.getDouble("price")  %>",
				"cid":"<%= rs.getInt("cid") %>"
			}
			<% 
				} 
				start = rs.next();
			}%>
		
		]
	}
	<%
		i ++;
	} 
	%>
	];

function pchainj(ii) {
	var notCatProdsSize = catProds.length + 1;
	if (ii < notCatProdsSize) {
		if (catProds[ii].products.length >0) {
		document.write("<table border=1 width='100%' style='margin-left:-42px;'>");
		
		document.write("<tr><td width='50%'>Name</td><td width='20%'>SKU</td><td width='15%'>Price</td><td width='15%'>Category</td></tr>")
	
		for (var i = 0; i < catProds[ii].products.length; i ++) {
			
			document.write("<tr><td><a href='order?id="+catProds[ii].products[i].SKU+"'><input type='hidden' name='skuu' value='" + catProds[ii].products[i].SKU + "'>"+catProds[ii].products[i].name+"</a></td>");
			document.write("<td>"+catProds[ii].products[i].SKU+"</td>");
			document.write("<td>"+catProds[ii].products[i].price+"</td>");
			document.write("<td>"+catProds[ii].category+"</td></tr>");
		}
	
		document.write("</table>");
		}  
	}
	else {
		if (catProds.length >0) {
			document.write("<table border=1 width='100%' style='margin-left:-42px;'>");
			
			document.write("<tr><td width='50%'>Name</td><td width='20%'>SKU</td><td width='15%'>Price</td><td width='15%'>Category</td></tr>")
		
			for (var i = 0; i < catProds.length; i ++) {
				for (var j = 0; j<catProds[i].products.length; j++) {
					document.write("<tr><td><a href='order?id="+catProds[i].products[j].SKU+"'><input type='hidden' name='skuu' value='" + catProds[i].products[j].SKU + "'>"+catProds[i].products[j].name+"</a></td>");
					document.write("<td>"+catProds[i].products[j].SKU+"</td>");
					document.write("<td>"+catProds[i].products[j].price+"</td>");
					document.write("<td>"+catProds[i].category+"</td></tr>");
				}
			}
		
			document.write("</table>");
			} 
	}
};

function cchange(ii) { 
	var sfield = $("#searchfield" + ii);
	var ta = "";
	var notCatProdsSize = catProds.length + 1;
	if (ii < notCatProdsSize) {
		
		if (catProds[ii].products.length >0) {
			ta += "<table border=1 width='100%' style='margin-left:-42px;'>";
			
			ta += "<tr><td width='50%'>Name</td><td width='20%'>SKU</td><td width='15%'>Price</td><td width='15%'>Category</td></tr>";
	
			var tabdel = $("#tabdel" + ii);
			tabdel.empty();
			
			for (var i = 0; i < catProds[ii].products.length; i ++) {
				if (catProds[ii].products[i].name.indexOf(sfield.val()) != -1) {
					
					ta +="<tr><td><a href='order?id="+catProds[ii].products[i].SKU+"'><input type='hidden' name='skuu' value='" + catProds[ii].products[i].SKU + "'>"+catProds[ii].products[i].name+"</a></td>";
					ta +="<td>"+catProds[ii].products[i].SKU+"</td>";
					ta +="<td>"+catProds[ii].products[i].price+"</td>";
					ta +="<td>"+catProds[ii].category+ "</td></tr>";
				}
			}
	
			ta +="</table>";
		}
	}
	else {
		if (catProds.length >0) {
			ta += "<table border=1 width='100%' style='margin-left:-42px;'>";
			
			ta += "<tr><td width='50%'>Name</td><td width='20%'>SKU</td><td width='15%'>Price</td><td width='15%'>Category</td></tr>";
	
			var tabdel = $("#tabdel" + ii);
			tabdel.empty();
			
			for (var i = 0; i < catProds.length; i ++) {
				for (var j = 0; j < catProds[i].products.length; j++)
					if (catProds[i].products[j].name.indexOf(sfield.val()) != -1) {
						
						ta +="<tr><td><a href='order?id="+catProds[i].products[j].SKU+"'><input type='hidden' name='skuu' value='" + catProds[i].products[j].SKU + "'>"+catProds[i].products[j].name+"</a></td>";
						ta +="<td>"+catProds[i].products[j].SKU+"</td>";
						ta +="<td>"+catProds[i].products[j].price+"</td>";
						ta +="<td>"+catProds[i].category+ "</td></tr>";
					}
			}
	
			ta +="</table>";
		}
	}
	tabdel.html(ta);

};

$(function() {
    $( "#tabs" ).tabs();
});

$(function() {
    $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
    $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
});
</script>

<style>
.ui-tabs-vertical .ui-tabs-nav {
	padding: .2em .1em .2em .2em;
	float: left;
	width: 12em;
}

.ui-tabs-vertical .ui-tabs-nav li {
	clear: left;
	width: 100%;
	border-bottom-width: 1px !important;
	border-right-width: 0 !important;
	margin: 0 -1px .2em 0;
}

.ui-tabs-vertical .ui-tabs-nav li a {
	display: block;
}

.ui-tabs-vertical .ui-tabs-nav li.ui-tabs-active {
	padding-bottom: 0;
	padding-right: .1em;
	border-right-width: 1px;
	border-right-width: 1px;
}

.ui-tabs-panel {
	margin: 0 auto;
	width: 600px;
	position: relative;
}

.wrapper {
	margin: 0 auto;
	width: 80%;
}

</style>

<%@ include file="private/middle.jsp"%>

<% if (session.getAttribute("role") != null && session.getAttribute("role").equals("customer")) { %>

	<% if ( session.getAttribute("loginstatus").equals("justsignedin") ) { 
		session.setAttribute("loginstatus", "loggedin");
	%>
	<div class="successDialog">
	You have successfully signed into <%= session.getAttribute("name") %>!
	</div>
	<% } else if ( session.getAttribute("loginstatus").equals("justregistered") ) { 
		session.setAttribute("loginstatus", "loggedin");
	%>
	<div class="successDialog">
	You have successfully registered the user <%= session.getAttribute("name") %>!
	</div>
	<% } %>

<h1>Browse Products</h1>

<div class="wrapper">
	<div id="tabs">
	  <ul>
	    <% for (int j = 1; j <= catNames.size(); j++) { %>
	    <li><a href="#tabs-<%= j %>"><%= catNames.get(j - 1) %></a></li>
	    <% } %>
	    <li><a href="#tabs-<%= catNames.size()+1 %>">All categories</a></li>
	  </ul>
	
	  <% for (int j = 1; j <= catNames.size(); j++) { %>
	  <div id="tabs-<%= j %>" class="element">
	    <h2>Current Products</h2>
	    Search Product: <input type="search" id="searchfield<%= j - 1 %>"
	      name="prodsearch">
	    <button value="Search" onclick="cchange(<%= j - 1 %>)">Search</button>
	    <br>
	    <br>
	    
	    <div id='tabdel<%= j-1 %>'>
	      <script>pchainj(<%= j-1 %>)</script>
	    </div>
	  </div>
	  <% } %>
	  <% int bigSize = catNames.size()+1; %>
	  <div id="tabs-<%= bigSize %>" class="element">
	    <h2>Current Products</h2>
	    Search Product: <input type="search" id="searchfield<%= bigSize %>"
	      name="prodsearch">
	    <button value="Search" onclick="cchange(<%= bigSize %>)">Search</button>
	    <br>
	    <br>
	    
	    <div id='tabdel<%= bigSize %>'>
	      <script>pchainj(<%= bigSize %>)</script>
	    </div>
	  </div>
	
	</div>
</div>

<% } else { /* The user is not logged in or not a customer */ %>

<div class="errorDialog">
	You must be logged in as a customer to view this page!
</div>

<% } %>

<%@ include file="private/ender.jsp"%>


