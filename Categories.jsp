<%@ include file="private/Library.jsp" %>

<% title = "Product Categories"; %>

<%
	//variable that checks if there are products in a category
	int checkProds = 0;

	//variable that checks for unique category name
	int checkCateg = 0;
	
	//Checking for Add Category button
	if (request.getParameter("add_categ") != null) {
		//Checking for non-null fields
		if (request.getParameter("category") != null && request.getParameter("desc") != null) {
			
			String catstr = request.getParameter("category");
			StringBuffer descstrBuff = new StringBuffer(request.getParameter("desc"));
			String descstr = descstrBuff.toString();
			
			if (catstr != "") {
				pstmt = conn.prepareStatement("SELECT name FROM categories WHERE name = ?");
				pstmt.setString(1,catstr);
				rs = pstmt.executeQuery();
				
				if(rs.next()) {
					checkCateg = 1;
					
				}
				else {
					pstmt = conn.prepareStatement("INSERT INTO categories (name, description) " + "VALUES (?, ?);");
					pstmt.setString(1,catstr);
					pstmt.setString(2,descstr);
					
					pstmt.executeUpdate();
				}
				
			}
		}
	}
	//Checking for Update Category button
	else if (request.getParameter("update_categ")!=null) {
		if (request.getParameter("newname")!=null) {
			String selectedCat = request.getParameter("update_categ_menu");
			StringBuffer newNameBuff = new StringBuffer(request.getParameter("newname"));
		   	String newName = newNameBuff.toString();
		   	
		   	pstmt = conn.prepareStatement("SELECT name FROM categories WHERE name = ?");
		   	pstmt.setString(1,newName);
		   	
		   	rs = pstmt.executeQuery();
		   	
		   	if (!(selectedCat.equals(newName)) && rs.next()) {
		   		checkCateg = 1;
		   	}
		   	else {
		   		pstmt = conn.prepareStatement("UPDATE categories SET name = ? WHERE name = ?");
		   		pstmt.setString(1,newName);
		   		pstmt.setString(2,selectedCat);
		   		
		   		pstmt.executeUpdate();
		   	}
		   	
		}
		if (request.getParameter("newdesc")!=null) {
		
			String selectedCat = request.getParameter("update_categ_menu");
			StringBuffer newNameBuff = new StringBuffer(request.getParameter("newname"));
		   	String newName = newNameBuff.toString();
		   	StringBuffer newDescBuff = new StringBuffer(request.getParameter("newdesc"));
		   	String newDesc = newDescBuff.toString();
		   	
		   	pstmt = conn.prepareStatement("UPDATE categories SET description = ? WHERE name = ?");
		   	pstmt.setString(1,newDesc);
		   	pstmt.setString(2,newName);
		   	
		   	pstmt.executeUpdate();
		}
	}
	//Checking for Delete Category button
	else if (request.getParameter("delete_categ")!=null) {
		
		int idval = -1;
		
		String delCat = request.getParameter("delete_categ_menu");
	   	pstmt = conn.prepareStatement("SELECT id FROM categories WHERE name = ?");
	   	pstmt.setString(1,delCat);
	   	rs = pstmt.executeQuery();
	   		
	   	if (rs.next()) {
	   		idval = rs.getInt("id");
	   	}
	   	
	   	pstmt = conn.prepareStatement("SELECT * FROM products WHERE cid = ?");
	   	pstmt.setInt(1,idval);
	   		
	   	rs = pstmt.executeQuery();
	   		
	   	if(rs.next()) {
	   		checkProds = 1;
	   	} 		
	   	else {
	   		pstmt = conn.prepareStatement("DELETE FROM categories WHERE id = ?");
	   		pstmt.setInt(1,idval);
	   		pstmt.executeUpdate();
	   	}
	}
%>

<%@ include file="private/header.jsp" %>
	
	<script>
		<%
		pstmt = conn.prepareStatement("SELECT name, description FROM categories ORDER BY id");
		rs = pstmt.executeQuery();
		%>
		
		ownersCat = [
			 <% 
			 ArrayList<String> cats = new ArrayList<String>(); 
			 
			 int q = 0;
			 while (rs.next()) {
				 if (q > 0) {
					 out.print(",");
				 }
				 String cname = rs.getString("name");
				 cats.add(cname);
				 %>
				 {
					 "title":"<%= cname %>",
					 <% 
					 if (rs.getString("description")!=null) { %>
						 "description":"<%= rs.getString("description") %>"
					 <%
					 }
					 else { %>
						 "description":""
					 <%
					 } %>
				 }
				 <%
				 q ++; 
			 } %>          
		];
		
		<%
		String initdesc = "";
		
		if (cats.size()>0) {
			pstmt = conn.prepareStatement("SELECT description FROM categories WHERE name = ?");
			String firstCat = cats.get(0);
			
			pstmt.setString(1,firstCat);
			rs = pstmt.executeQuery();
			
			if (rs.next()) {
				if (rs.getString("description") == null) {
					initdesc = "";
				}
				else {
					initdesc = rs.getString("description");
				}
			}
		}
		%>
		
		$(function() {
		$( "#tabs" ).tabs();
		});
		
		
		function changedescripPara(select_id, desc_id) {
			var val = document.getElementById(select_id).selectedIndex;
			document.getElementById(desc_id).innerHTML=ownersCat[val].description;
		};
		
		function changeUpdateInput(select_id, name_id, desc_id) {
			var val = document.getElementById(select_id).selectedIndex;
			document.getElementById(name_id).value = ownersCat[val].title;
			document.getElementById(desc_id).value = ownersCat[val].description;
		};
		
	</script>

<%@ include file="private/middle.jsp" %>

<% if (session.getAttribute("role") != null && session.getAttribute("role").equals("owner")) { %>
	
	<h1>Categories</h1><br><br>
	<%
	if (request.getParameter("add_categ")!=null) { 
		if (checkCateg == 0) { %>
			<div class="successDialog">Successfully inserted category.<br>
				Category: <%= request.getParameter("category")  %><br>
				<% StringBuffer text = new StringBuffer(request.getParameter("desc")); %>
				Description: <%= text %>
			</div>
		<%
		}
		else { %>
			<div class="errorDialog">
				The category, <%= request.getParameter("category")  %>, already exists.<br>
				Please use a category name that has not been used yet.
			</div>
		<%
		}
	} 
	else if (request.getParameter("update_categ")!=null) { 
		
		String oldName = request.getParameter("update_categ_menu");
		
		if (request.getParameter("newname") != null) { 
			StringBuffer nametextBuff = new StringBuffer(request.getParameter("newname"));
			String nametext = nametextBuff.toString();
			if (checkCateg==0) { %>
				<div class="successDialog">Successfully updated category.<br>
					Category: <%= request.getParameter("update_categ_menu")  %><br>
					<%
					if (!oldName.equals(nametext)) { %>
						New Name: <%= nametext %><br>
					<%
					}
					StringBuffer text = new StringBuffer(request.getParameter("newdesc")); %>
					New Description: <%= text %>
				</div>
			<%
			}
			else { %>
				<div class="errorDialog">
					The category name, <%= nametext %>, already exists.<br>
					Please use a category name that has not been used yet.
				</div>
			<%	
			}
		}
	} 
	else if (request.getParameter("delete_categ")!=null) { 
		if (checkProds == 0) { %>
			<div class="successDialog">Successfully deleted category: <%= request.getParameter("delete_categ_menu")  %></div>
		<%
		}
		else { %>
			<div class="errorDialog">The category, <%= request.getParameter("delete_categ_menu")  %>, cannot be deleted because there are products within this category.</div>
		<%
		}
	} %>
  <div style="width: 80%; margin: 0 auto;">	 
	<div id="tabs">
	
	  	<ul>
		    <li><a href="#insert">Insert</a></li>
		    <li><a href="#update">Update</a></li>
		    <li><a href="#delete">Delete</a></li>
	  	</ul>
	  	
	  	<div id="insert">
	    	<form name="input" action="categories" method="POST">
		   		Name: <input type="text" name="category"><br>
		      	<br> Description: 
		      	<input type="text" name="desc">
		      	<br><br>
		      	<input type="submit" name="add_categ" value="Submit">
	   		 </form>
	  	</div>
	  	
	 	<div id="update">    
	    	<form action="categories" method="post">
	    		Select Category: 
	     		<select id="updateSelect" name="update_categ_menu" onchange="changeUpdateInput('updateSelect','updateName','updateDesc')">
		      		<% for (int w = 0; w < cats.size(); w++) { %>
		      			<option value="<%= cats.get(w) %>"><%= cats.get(w) %></option>
		      		<% } %>
	      		</select> <br><br> 
	      		New Name:
	      		<input name="newname" value="<% out.println(cats.get(0)); %>" type="text" id="updateName">
	      		<br><br>
	      		
	      		New Description: 
	      		<input name="newdesc" value="<% out.println(initdesc); %>" type="text" id="updateDesc">
	     		<br><br>
	     		<input type="submit" name="update_categ" value="Update">
	   		 </form>
	  	</div>
	  	
	  	<div id="delete">
			<form action="categories" method="post">
				Select Category: 
	      		<select id="deleteSelect" name="delete_categ_menu" onchange="changedescripPara('deleteSelect','updateDesc2')">
		        	<% for (int z = 0; z < cats.size(); z++) { %>
		      			<option value="<%= cats.get(z) %>"><%= cats.get(z) %></option>
		      		<% } %>
			    </select> 
			    <br>
	      		<p id="updateDesc2"><% out.println(initdesc); %></p>
	      		
	     		<input type="submit" name="delete_categ" value="Delete">
	    		</form>
	  	</div>
	</div>
  </div>

<% } else { %>
	 
	<div class="errorDialog">
		You must be logged in as an owner to view this page!
	</div>
	
<% } %> 
    
  
<%@ include file="private/ender.jsp" %>