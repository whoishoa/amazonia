<%@ include file="private/Library.jsp"%>

<% title = "Buy Shopping Cart"; %>

<%
	int user_id = -1;
	String user_name = null;
	String user_state = null;
	double formattemp = 0.0;
	double formatfinal = 0.0;
	int temp_sum = 0;
	int final_sum = 0;

	int temp_cid = -1;
	int temp_pid = -1;
	int temp_quantity = 0;
	int temp_price = 0;
	int temp_sid = -1;
	
	if (session.getAttribute("user_id")!=null){
		user_id = (Integer) session.getAttribute("user_id");
	}
	if (session.getAttribute("name")!= null) {
		user_name = (String) session.getAttribute("name");
	}
	if (session.getAttribute("state")!=null) {
		user_state = (String) session.getAttribute("state");
		pstmt = conn.prepareStatement("SELECT id FROM states WHERE name = ?");
		pstmt.setString(1, user_state);
		
		rs = pstmt.executeQuery();
		
		if (rs.next()) {
			temp_sid = rs.getInt("id");
		}
	}
	
%>

<%@ include file="private/header.jsp" %>
<%@ include file="private/middle.jsp" %>
		<h1>Confirmation</h1>
		
		<div class="successDialog">
			Congratulations! You have successfully made your purchase.
		</div>
		
		  <table border=1 cellpadding=5 align="center">
          <tr>
            <th>Product Name</th>
            <th>SKU</th>
            <th>Quantity</th>
			<th>Price</th>
			<th>Amount Price</th>
          </tr>
				<%//get user's cart
				pstmt = conn.prepareStatement("SELECT p.id, p.cid, p.name, p.SKU, c.quantity, p.price "
												+"FROM products AS p,carts AS c "
												+"WHERE c.uid = ? "
												+"AND c.pid = p.ID");
												
				pstmt.setInt(1, user_id);
				rs = pstmt.executeQuery();
				while(rs.next()) { 
					
					temp_cid = rs.getInt("cid");
					temp_pid = rs.getInt("id");
					temp_price = rs.getInt("price");
					temp_quantity = rs.getInt("quantity");
					temp_sum = temp_price*temp_quantity;
					final_sum+=temp_sum;
					
					formattemp = (double) temp_sum;
					formatfinal = (double) final_sum;
				%>
				<tr>
					<td><%=rs.getString("name")%></td>
					<td><%=rs.getString("SKU")%></td>
					<td><%=temp_quantity%></td>
					<td>$<%=formatPrice(temp_price)%></td>
					<td>$<%=formatPrice(formattemp)%></td>
				</tr>
				
				<%
				
				pstmt = conn.prepareStatement("INSERT INTO sales (uid, pid, quantity, price) "
												+"VALUES("+user_id+", "+temp_pid+", "+temp_quantity+", "
												+temp_price+")");
				
				pstmt.executeUpdate();
				
				pstmt = conn.prepareStatement("DO $do$ BEGIN "
												+"IF EXISTS (SELECT pid FROM preproducts "
												+"WHERE pid="+temp_pid+") "
												+"THEN "
												+"UPDATE preproducts "
												+"SET sum=sum+"+temp_sum+" "
												+"WHERE pid="+temp_pid+"; "
												+"ELSE "
												+"INSERT INTO preproducts (pid, sum) "
												+"VALUES("+temp_pid+", "+temp_price+"); "
												+"END IF; " 
												+"END $do$");
				pstmt.executeUpdate();
				
				pstmt = conn.prepareStatement("DO $do$ BEGIN "
												+"IF EXISTS (SELECT sid, cid FROM prestatescategories "
												+"WHERE sid="+temp_sid+" AND cid="+temp_cid+") " 
												+"THEN "
												+"UPDATE prestatescategories "
												+"SET sum=sum+"+temp_sum+" "
												+"WHERE sid="+temp_sid+" AND cid="+temp_cid+"; "
												+"ELSE "
												+"INSERT INTO prestatescategories (sid, cid, sum) "
												+"VALUES("+temp_sid+", "+temp_cid+", "+temp_sum+"); "
												+"END IF; " 
												+"END $do$");
				pstmt.executeUpdate();
				
				
				pstmt = conn.prepareStatement("DO $do$ BEGIN "
												+"IF EXISTS (SELECT sid, pid FROM prestatesproducts "
												+"WHERE sid="+temp_sid+" AND pid="+temp_pid+") " 
												+"THEN "
												+"UPDATE prestatesproducts "
												+"SET sum=sum+"+temp_sum+" "
												+"WHERE sid="+temp_sid+" AND pid="+temp_pid+"; "
												+"ELSE "
												+"INSERT INTO prestatesproducts (sid, pid, sum) "
												+"VALUES("+temp_sid+", "+temp_pid+", "+temp_sum+"); "
												+"END IF; " 
												+"END $do$");
				pstmt.executeUpdate();
				
				pstmt = conn.prepareStatement("DO $do$ BEGIN "
												+"IF EXISTS (SELECT uid, cid FROM preuserscategories "
												+"WHERE uid="+user_id+" AND cid="+temp_cid+") " 
												+"THEN "
												+"UPDATE preuserscategories "
												+"SET sum=sum+"+temp_sum+" "
												+"WHERE uid="+user_id+" AND cid="+temp_cid+"; "
												+"ELSE "
												+"INSERT INTO preuserscategories (uid, cid, sum) "
												+"VALUES("+user_id+", "+temp_cid+", "+temp_sum+"); "
												+"END IF; " 
												+"END $do$");
				pstmt.executeUpdate();
			

				pstmt = conn.prepareStatement("DO $do$ BEGIN "
												+"IF EXISTS (SELECT uid, pid FROM preusersproducts "
												+"WHERE uid="+user_id+" AND pid="+temp_pid+") "
												+"THEN "
												+"UPDATE preusersproducts "
												+"SET sum=sum+"+temp_sum+" "
												+"WHERE uid ="+user_id+" AND pid="+temp_pid+"; "
												+"ELSE "
												+"INSERT INTO preusersproducts (uid, pid, sum) "
												+"VALUES("+user_id+", "+temp_pid+", "+temp_price+"); "
												+"END IF; " 
												+"END $do$");
				pstmt.executeUpdate();
				
				%>
				
				<% } %>
				</table>
				
				<br>
				Grand Total: $<%= formatPrice(formatfinal) %>
				<br />
				
				<%
				pstmt = conn.prepareStatement("DO $do$ BEGIN "
												+"IF EXISTS (SELECT sid FROM prestates "
												+"WHERE sid="+temp_sid+") "
												+"THEN "
												+"UPDATE prestates "
												+"SET sum=sum+"+final_sum+" "
												+"WHERE sid="+temp_sid+"; "
												+"ELSE "
												+"INSERT INTO prestates (sid, sum) "
												+"VALUES("+temp_sid+", "+final_sum+"); "
												+"END IF; " 
												+"END $do$");
				pstmt.executeUpdate();
				
				
				pstmt = conn.prepareStatement("DO $do$ BEGIN "
												+"IF EXISTS (SELECT uid FROM preusers "
												+"WHERE uid="+user_id+") "
												+"THEN "
												+"UPDATE preusers "
												+"SET sum=sum+"+final_sum+" "
												+"WHERE uid="+user_id+"; "
												+"ELSE "
												+"INSERT INTO preusers (uid, sum) "
												+"VALUES("+user_id+", "+final_sum+"); "
												+"END IF; " 
												+"END $do$");
				pstmt.executeUpdate();
				
				//delete users cart:
				pstmt = conn.prepareStatement("DELETE FROM carts "
												+ "WHERE uid = ?" );
				pstmt.setInt(1, user_id);
				pstmt.executeUpdate();
				%>
				
		<br />
		<form method="POST"action="browse">
			<input type="submit" name="submit_button" value="Go Back To Product Browsing" />
		</form>
  </body>
</html>
