<%@ include file="private/Library.jsp" %>

<% title = "New Testing Laboratory"; %>

<%
boolean wasLoggedIn = session.getAttribute("user_id") != null;
session.setAttribute("user_id", null);
session.setAttribute("name", null);
session.setAttribute("role", null);
session.setAttribute("age", null);
session.setAttribute("state", null);

%>

<%@ include file="private/header.jsp" %>
<%@ include file="private/middle.jsp" %>

<% if (wasLoggedIn) { %>
	<div class="successDialog">
		You have successfully signed out!
	</div>
<% } else { %>
	<div class="noteDialog">
		You are already signed out!
	</div>
<% } %>

<img src="imgs/loggedout.png" style="height:450px;margin-top:20px">
<br />
<button id="registerButton" class="mainButton" onclick="window.location='login'">
	Log Back In
</button>

<%@ include file="private/ender.jsp" %>