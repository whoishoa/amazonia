<%@ include file="private/Library.jsp" %>

<% title = "My One Class Login"; %>

<%
String invalidUser = ""; //will set if invalid
// See if submit button was pressed
if(request.getParameter("signed_up") != null){

	pstmt = conn.prepareStatement("SELECT * FROM users WHERE name = ?");
	String name = request.getParameter("name");
	pstmt.setString(1, name);
	rs = pstmt.executeQuery();
	//check if the student exists:
	if(rs.next()) {
		//store session attributes if user does exist. And go to next page.
		int user_id = rs.getInt("ID");
		name = rs.getString("name");
		String role = rs.getString("role");
		String age = rs.getString("age");
		String state = rs.getString("state");
		session.setAttribute("user_id", user_id);
		session.setAttribute("name", name);
		session.setAttribute("role", role);
		session.setAttribute("age", age);
		session.setAttribute("state", state);
		session.setAttribute("loginstatus", "justsignedin");
		
		if (role.equals("customer")) {
			response.sendRedirect("browse"); //go to next page
		} else {
			response.sendRedirect("products");
		}
	} else {
		invalidUser = "The provided name " + name +" is not known";
	}
}
%>

<%@ include file="private/header.jsp" %>
<script>

window.onload = function() {
	PT.create("accountModal",{
		associate:"name",
		bgColor:"#00AAFF",
		pointing: "left",
		position: "right",
		showing: true,
		active: false,
		distance: 30
		});
}

</script>
<%@ include file="private/middle.jsp" %>

<% if (invalidUser.length() > 0) { %>
<div class="errorDialog">
	<%= invalidUser %>
</div>	
<% } %>

<h1>Log In To Your Account</h1>
<div id="loginDiv">
	<img src="imgs/school.png" style="width:90%;margin-top:10px;">
	<% if (session.getAttribute("user_id") != null) { %>
		<div class="alertBox">
			You are already logged in as <%= session.getAttribute("name") %>. 
			If you log in to another user, you will automatically out of your current user.
		</div>
	<% } %>

	<form method="POST"action="login">
		<h3>Username</h3>
		<input id="name" class="registerInput" type="text" size="20" name="name" autofocus/>
		<br />
		<input id="registerButton" type="submit" class="mainButton" name="signed_up" value="Sign In" />
    </form>
	
	<div id="accountModal">
		<div class="clickable full padd10" onclick="window.location='signup'">
			Need an account? Click here!
		</div>
	</div>
</div>
	
<%@ include file="private/ender.jsp" %>