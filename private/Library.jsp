<%@ page import="java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ page import="java.text.*"%>
<%@ page import="java.math.*"%>
<%@ page import="java.util.*"%>

<%!
  /** Constants */
  public static final String DB_LOC = 
      //"jdbc:postgresql://54.187.86.67/cse135?user=postgres&password=postgres";
	  "jdbc:postgresql://localhost:5432/cse135?user=postgres&password=postgres";

  public String title = "My One Class' Store";
  public String[] cssImports = {};
  public String[] jsImports = {};

  //convert price to have exactly 2 decimals, return as string for display.
  public String formatPrice(double d) {
    NumberFormat df = DecimalFormat.getInstance();
    df.setMinimumFractionDigits(2);
    df.setMaximumFractionDigits(2);
    df.setRoundingMode(RoundingMode.DOWN);
    
    return df.format(d);
  }
  
  public boolean isAlphaNumeric(String name) {
	for (int i = 0; i < name.length(); i++) {
		if (! Character.isLetterOrDigit(name.charAt(i))) {
			return false;
		}
	}
	return true;
  }
  
  public String isValidRegistry(String name, int age, String state, String role) {
	if ( ! (name != null && name.length() > 1 && name.length() <= 64) ) {
		return "The username must be between 1 and 64 characters";
	} 
	if (! isAlphaNumeric(name)) {
		return "The username must only contain letters or numbers. There can't be any whitespace.";
	}
	if(age < 0 || age > 150) { //if there is a user with this name, or age is out of bounds, return false	
	   String retval = "Age must be an number between 0 and 150";
           return retval;
	} else if (!role.equals("C") && !role.equals("O")) {
		return "Invalid role entered.";
	} else if (state.length() != 2) {
		return "Invalid state entered.";
	} else {
		return "success";
    }
  }
  
  public String p(Object j) {
	if (j == null) {
		return "";
	}
	return j.toString();
  }
%>
<%
	Connection conn = null;
    PreparedStatement pstmt = null;
	ResultSet rs = null;
	ResultSet rs2 = null; //secondary.
	ResultSet rs3 = null;
    Class.forName("org.postgresql.Driver");
    conn = DriverManager.getConnection(DB_LOC);
%>
