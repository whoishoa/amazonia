  </head>
  <body>
    <div id="headerDiv">
	  <div id="headerMenu" onclick="toggleMenu()"><img src="imgs/menu.png"></div>
      <img id="headerTitle" src="imgs/title.png" class="clickable" onclick="window.location=''">
	  
	  <% if (session.getAttribute("user_id") != null) { %>
	  
		  <% if (session.getAttribute("role").equals("customer")) { %>
		  <div id="headerCart" onclick="window.location='cart'"><img src="imgs/cart.png"></div>
		  <div id="cartTooltipModal">Buy Shopping Cart</div>
		  
		  <% } else { %>
		  <div id="headerFiller"></div>
		  <% } %>
		<div id="helloBubbleModal">Hello <%= session.getAttribute("name") %>!</div>
	  <% } else { %>
	  <div id="headerUser">
	    <div class="headerUserWrapper">
			<button id="headerLogin" class="headerUserButton" onclick="window.location='login'">
				Log In
			</button>
		</div>
		<div id="headerLoginSpacing">
		</div>
		<div class="headerUserWrapper">
			<button id="headerSignup" class="headerUserButton" onclick="window.location='signup'">
				Sign Up
			</button>
		</div>
	  </div>
	  <% } %>
    </div> 
	<div id="contentDiv">