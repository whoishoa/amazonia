    </div>
    <div id="enderdiv">
	  <div id="broughtToYouBy">Brought to you by:</div><br />
	  <div class="creditsimg" id="adamsprite"></div>
	  <div class="creditsimg" id="hoasprite"></div>
	  <div class="creditsimg" id="viviansprite"></div><br />
	  <img class="labelimg" src="imgs/Adamlabel.png">
	  <img class="labelimg" src="imgs/hoalabel.png">
	  <img class="labelimg" src="imgs/vivianlabel.png"><br />
	  <div id="copyrightMessage">Copyright &copy; 2014-Present by MyOneClass.com. All trademarks are property of MyOneClass.com.</div>
	</div>
    <div id="menuModal">
		<div id="menuArrow"></div>
		<div id="menuTopOffset"></div>
		<% if (session.getAttribute("name") != null) { %>
			<!-- <div class="menuCategory">Products</div> -->
			
			
			
			<% if (session.getAttribute("role").equals("owner")) { %>
			<div class="menuOption" onclick="window.location='products'">
				<div id="prodAddSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Add/Search Products</div>
			</div>
			
			<!-- <div class="menuCategory">Categories</div> -->
			<!--
			<div class="menuOption" onclick="window.location='categories#add'">
				<div id="catAddSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Add</div>
			</div>
			-->
			<div class="menuOption" onclick="window.location='categories'">
				<div id="catUpdateSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Categories</div>
			</div>
			<!--
			<div class="menuOption" onclick="window.location='categories#delete'">
				<div id="catDeleteSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Delete</div>
			</div>
			-->
			
			<div class="menuOption" onclick="window.location='analytics'">
				<div id="prodOrdersSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Analytics</div>
			</div>
			
			
			<% } else { %>
			
			<div class="menuOption" onclick="window.location='browse'">
				<div id="prodBrowseSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Browse Products</div>
			</div>
			<!--
			
			<div class="menuOption" onclick="window.location='order'">
				<div id="prodOrdersSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Orders</div>
			</div>
			
			-->
			<div class="menuOption" onclick="window.location='cart'">
				<div id="prodCartSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Cart</div>
			</div>
			
			<% } %>
			<!-- <div class="menuCategory">User</div> -->
			<div class="menuOption" onclick="window.location='signout'">
				<div id="signOutSprite" class="menuOptionPic"></div>
				<div class="menuOptionLabel">Sign Out</div>
			</div>
		<% } else { %>
            <div class="menuOption" onclick="window.location='http://www.myoneclass.com/login'">
              <div id="signOutSprite" class="menuOptionPic"></div>
              <div class="menuOptionLabel">Please log in first</div>
            </div>
		<% } %>
	</div>
    </div>
  </body>
</html>