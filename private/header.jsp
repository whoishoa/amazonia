<!DOCTYPE html>
<html>
  <head>
    <title><%= title %></title>
    <!-- <base href="http://myoneclass.com/" target="_blank"> -->
    <link rel="icon" type="image/png" href="imgs/favicon.png">
	
	<!-- CSS IMPORTS -->
    <link rel="stylesheet" type="text/css" href="css/main.css?a" />
    <link rel="stylesheet" type="text/css" href="css/common.css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
    <% for (int i = 0; i < cssImports.length; i++) { %>
	<link rel="stylesheet" href="<%= cssImports[i] %>">
	<% } %>
	
	<!-- JS IMPORTS -->
	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
	<script src="js/pointtip.js"></script>
	<script src="js/library.js"></script>
	<% for (int i = 0; i < jsImports.length; i++) { %>
	<script src="<%= jsImports[i] %>"></script>
	<% } %>
	
