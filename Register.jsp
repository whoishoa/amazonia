<%@ include file="private/Library.jsp" %>

<% 
title = "New Account";
String[] cssFiles = {"http://ivaynberg.github.io/select2/select2-3.4.6/select2.css"};
cssImports = cssFiles;
String[] jsFiles = {"http://ivaynberg.github.io/select2/select2-3.4.6/select2.js"};
jsImports = jsFiles;

String resp = "";

if(request.getParameter("registerButton") != null){ 
	String name = request.getParameter("name");
	String role = request.getParameter("role");
	int age = -1;
	try {
		age = Integer.parseInt( request.getParameter("age") );
	} catch (Exception e) {
		
	}
	String state = request.getParameter("state");
	
	resp = isValidRegistry(name, age, state, role);
	if (resp.equals("success")) {
		pstmt = conn.prepareStatement("SELECT name FROM users WHERE users.name = ?");
		pstmt.setString(1, name);
		rs = pstmt.executeQuery();
		if (rs.next()) {
			resp = "Sorry, the username " + name + " is already taken!";
		} else {
			pstmt = conn.prepareStatement("INSERT INTO users (name, role, age, state) "
										+ "VALUES (?, ?, ?, ?);");

			pstmt.setString(1, name);
			pstmt.setString(2, role);
			pstmt.setInt(3, age);
			pstmt.setString(4, state);
			pstmt.executeUpdate();
		
			// Logs the user in
			pstmt = conn.prepareStatement("SELECT ID FROM users WHERE name = ?");
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			rs.next();
			int user_id = rs.getInt("ID");
			
			session.setAttribute("user_id", user_id);
			session.setAttribute("name", name);
			session.setAttribute("role", role);
			session.setAttribute("age", age);
			session.setAttribute("state", state);
			session.setAttribute("loginstatus", "justregistered");
		
            if (role.equals("O")) {
			   //redirect to the sign-in page.
			   response.sendRedirect("products");
            } else {
               response.sendRedirect("browse");
            }
		}
	}	
}
%>
 
<%@ include file="private/header.jsp" %>

<style>
.flag {
   height:10px;
}

</style>

<script>

function format(state) {
  if (!state.id) return state.text;
  return state.text;
}

window.onload = function() {
  $("#state").select2({
    formatResult: format,
    formatSelection: format,
    escapeMarkup: function(m) { return m; }
  });
}
</script>

<%@ include file="private/middle.jsp" %>
<% if (session.getAttribute("user_id") != null) { %>
		<div class="noteDialog">
			You are currently logged in as <%= session.getAttribute("name") %>.<br /><br />
			If you successfully register another user, you will automatically out of your current user and logged into the new user.
		</div>
<% } %>

    <h1>Create your My One Class Account</h1>
	<br />
	
	<div id="registerWrapper">
		<div id="whyRegisterDiv">
		</div>
		<div id="registerFormDiv">
			<form method="POST" action="signup">
				<h3>Username:</h3>
				<input type="text" class="registerInput" name="name" value="<%= p(request.getParameter("name")) %>"/>
				<br />
				<h3>Role:</h3>
				<select name="role" class="registerInput registerSelect paddSide5">
					<option value="customer">Customer</option>
					<option value="owner">Owner</option>
				</select>
				<br />
				<h3>Age:</h3>
				<input class="registerInput" type="text" name="age" value="<%= p(request.getParameter("age")) %>"/>
				<br />
				<h3>State:</h3>
				<select class="registerSelect" id="state" name="state">
					<optgroup label="Alaskan/Hawaiian Time Zone">
						<option value="Alaska">Alaska</option>
						<option value="Hawaii">Hawaii</option>
					</optgroup>
					<optgroup label="Pacific Time Zone">
						<option value="California">California</option>
						<option value="Nevada">Nevada</option>
						<option value="Oregon">Oregon</option>
						<option value="Washington">Washington</option>
					</optgroup>
					<optgroup label="Mountain Time Zone">
						<option value="Arizona">Arizona</option>
						<option value="Colorado">Colorado</option>
						<option value="Idaho">Idaho</option>
						<option value="Montana">Montana</option
						option value="Nebraska">Nebraska</option>
						<option value="New Mexico">New Mexico</option>
						<option value="North Dakota">North Dakota</option>
						<option value="Utah">Utah</option>
						<option value="Wyoming">Wyoming</option>
					</optgroup>
					<optgroup label="Central Time Zone">
						<option value="Alabama">Alabama</option>
						<option value="Arkansas">Arkansas</option>
						<option value="Illinois">Illinois</option>
						<option value="Iowa">Iowa</option>
						<option value="Kansas">Kansas</option>
						<option value="Kentucky">Kentucky</option>
						<option value="Louisiana">Louisiana</option>
						<option value="Minnesota">Minnesota</option>
						<option value="Mississippi">Mississippi</option>
						<option value="Missouri">Missouri</option>
						<option value="Oklahoma">Oklahoma</option>
						<option value="South Dakota">South Dakota</option>
						<option value="Texas">Texas</option>
						<option value="Tennessee">Tennessee</option>
						<option value="Wisconsin">Wisconsin</option>
					</optgroup>
					<optgroup label="Eastern Time Zone">
						<option value="Connecticut">Connecticut</option>
						<option value="Delaware">Delaware</option>
						<option value="Florida">Florida</option>
						<option value="Georgia">Georgia</option>
						<option value="Indiana">Indiana</option>
						<option value="Maine">Maine</option>
						<option value="Maryland">Maryland</option>
						<option value="Massachusetts">Massachusetts</option>
						<option value="Michigan">Michigan</option>
						<option value="New Hampshire">New Hampshire</option>
						<option value="New Jersey">New Jersey</option>
						<option value="New York">New York</option>
						<option value="North Carolina">North Carolina</option>
						<option value="Ohio">Ohio</option>
						<option value="Pennsylvania">Pennsylvania</option>
						<option value="Rhode Island">Rhode Island</option>
						<option value="South Carolina">South Carolina</option>
						<option value="Vermont">Vermont</option>
						<option value="Virginia">Virginia</option>
						<option value="West Virginia">West Virginia</option>
					</optgroup>
				</select>
			    <h3>Note:</h3>
				<div id="registerNote">
				By registering for an account, you agree to use MyOneClass in a responsible manner.
				</div>
				
				<% if (resp.length() != 0) { %>
					<div id="registerError">
						<span class="bold">Your signup failed</span>
						<br><br>
						<%= resp %>
					</div>
				<% } else { %>
					<div id="registerError" class="invisible">
						<br><br>
					</div>
				<% } %>
				
			    <input type="submit" id="registerButton" name="registerButton" class="mainButton" value="Register Account" />
				
			</form>
		</div>
	</div>
 
<%@ include file="private/ender.jsp" %>
