function $id(id) {
	return document.getElementById(id);
}

function $attr(id, attribute, value) {
	if (value === undefined) {
		var val = $id(id).getAttribute(attribute);
		if (val == null) {
			return '';
		}
		return val;
	}
	$id(id).setAttribute(attribute, value);
}

function $changeClass(identifier, oldClass, newClass) {
	$(identifier).removeClass(oldClass);
	$(identifier).addClass(newClass);
}

// Adds the part to the beginning of the attribute
function $addAttrPart(id, attribute, part) {
	$attr(id, attribute, part + $attr(id, attribute));
}

function $removeAttrPart(id, attribute, part) {
	var oldAttri = $id(id).getAttribute(attribute);
	if (oldAttri == null) {
		return;
	}
	var oldAttriIndi = oldAttri.indexOf(part);
	if (oldAttriIndi != -1) {
		var newAttribute = oldAttri.substr(0, oldAttriIndi);
		newAttribute += oldAttri.substr(oldAttriIndi + part.length);
		$attr(id, attribute, newAttribute);
	}
}

/* Used to highlight the text links at the bottom of the site */
function textLinkHL(id) {
	$("#" + id).css("text-decoration", "underline");
	$("#" + id).css("color", "black");
}

/* Used to dehighlight the text links at the bottom of the page */
function textLinkDL(id) {
	$("#" + id).css("text-decoration", "");
	$("#" + id).css("color", "");
}

function PointTipManager() {
	this.defaultModalsDivId = "modalsDiv";
	
	this.instances = {};
	
	/* 
	 * Fully functional pointy tooltip library
	 * Parameters: 
	 *     String divid - the id of the div that will be put
	 *         as the content of the tooltip
	 *     Object options - Allows for customization of the
	 *         pointy tooltip.
	 * Valid Options:
	 *     String pointing - "up", "right", "down", "left", 
	 *         "none", or "dynamic"
	 *     String position - "top", "bottom", "right", "left",
	 *         "middle", "dynamic" relative to the associated 
	 *         element
	 *     String distance - distance in pixels from the
	 *         associated element.
	 *     String arrowClass - Class that describes the
	 *         pointing arrow's appearance.
	 *     String contentClass - Class that describes the
	 *         tooltip's content appearance
	 *     String bgcolor - background color of the tooltip
	 *     String fontcolor - the color of the text
	 *     int curve - How curvy should the edges be in pixels
	 *     String id - the id associated with this tooltip
	 *     String associate - The object that this tooltip will
	 *         be associated with.
	 *     boolean showing - Whether the tooltip is visible
	 *         or not
	 *     int top - The initial top offset of the tooltip
	 *     int left - The initial left offset of the tooltip
	 *     boolean active - Whether or not the pointy tooltip
	 *         should fade in and out when the mouse hovers
	 *         over the associated element.
	 *     int fade - in milliseconds, the amount of time it
	 *         takes this tooltip to fade in or out
	 *     int fadein - in milliseconds, the amount of time it
	 *         takes this tooltip to fade in
	 *     int fadeout - in milliseconds, the amount of time it
	 *         takes this tooltip to fade out
	 *     int graceperiod - time in milliseconds after the cursor
	 *         goes on the associated element before the 
	 *         tooltip fades in
	 *     int endperiod - time in milliseconds after the cursor
	 *         leaves the associated element before the tooltip 
	 *         fades out
	 */
	function PointTip(divid, options) {
		var defaultModalsDiv = $("#" + PT.defaultModalsDivId);
		if (defaultModalsDiv.length == 0) {
			$("body").append("<div id='" + PT.defaultModalsDivId + "'></div>");
			var defaultModalsDiv = $("#" + PT.defaultModalsDivId);
		}
		var contents = $("#" + divid);
		if (contents.length == 0) {
			console.error("PointTip Constructor Error: " + divid + 
				" is not an id of a document element.");
			return;
		}
		this.id = divid;
		// Default values
		this.position = "dynamic";
		this.distance = 20;
		this.pointing = "dynamic";
		this.arrowClass = "pointTipArrow";
		this.contentClass = "pointTipContent";
		this.onEvent = "onmouseover";
		this.offEvent = "onmouseout";
		this.bgColor = "orange";
		this.fontColor = "white";
		this.curve = 0;
		this.associate = false;
		this.showing = false;
		this.top = -1;
		this.left = -1;
		this.active = true;
		this.fade = 500;
		this.outFade = -1;
		this.inFade = -1;
		this.graceperiod = 0;
		this.endperiod = 0;
		this.arrowEdgeSize = 10;
		this.arrowBaseSize = 10;
		this.dynamoDirection = "left";
		
		this.newlyCreated = true;
		
		// Mouse is on the associated element
		this.associateMouseOn = false;
		// Element is currently not fading
		this.available = true;
		// Element is visible as in opacity > 0
		this.on = false;
		
		var htmlConstruct = 
			"<table id='" + this.id + "' class='condense modal' style='top:" + 
			    this.top + "px;left:" + this.left + "px;opacity:0;display:none'>" +
			  "<tr id='" + this.id + "ContentTR' class='condense'>" +
			    "<td id='" + this.id + "ArrowTD' class='condense'>" +
			      "<div id='" + this.id + "Arrow' class='" + this.arrowClass + "'></div>" +
			    "</td>" +
			    "<td id='" + this.id + "ContentTD' class='" + this.contentClass + "'>" +
			      contents.html() +
			    "</td>" +
			  "</tr>" +
			"</table>";
		defaultModalsDiv.append(htmlConstruct);
		contents.remove();
		
		this.instance = $('#' + this.id);
		PT.instances[this.id] = this;
		
		this.POINTING_OPTIONS = {
				dynamic:function(hasNewPointing) {
					return this.left;
				},
				none:function(hasNewPointing) {
					// Currently, there is no need to do anything :)
				},
				up:function(src, toPrependIgnore, toPrependHTML, hasNewPointing) {
					if (hasNewPointing) {
						 $('#' + src.id).prepend("<tr id='" + src.id + 
								   "ArrowTR' class='condense'>" + toPrependHTML + "</tr>");
					}
					var arrow = $("#" + src.id + "Arrow");
					arrow.css("border-bottom", src.arrowBaseSize + "px solid " + src.bgColor);
					arrow.css("border-right", src.arrowEdgeSize + "px solid transparent");
					arrow.css("border-left", src.arrowEdgeSize + "px solid transparent");
				},
				left:function(src, toPrepend, toPrependHTML, hasNewPointing) {
					if (hasNewPointing) {
						toPrepend.prepend(toPrependHTML);
					}
					var arrow = $("#" + src.id + "Arrow");
					arrow.css("border-right", src.arrowBaseSize + "px solid " + src.bgColor);
					arrow.css("border-top", src.arrowEdgeSize + "px solid transparent");
					arrow.css("border-bottom", src.arrowEdgeSize + "px solid transparent");
				},
				down:function(src, toIgnore, toAppendHTML, hasNewPointing) {
					if (hasNewPointing) {
						 $('#' + src.id).append("<tr id='" + src.id + 
								   "ArrowTR' class='condense'>" + toAppendHTML + "</tr>");
					}
					var arrow = $("#" + src.id + "Arrow");
					arrow.css("border-top", src.arrowBaseSize + "px solid " + src.bgColor);
					arrow.css("border-right", src.arrowEdgeSize + "px solid transparent");
					arrow.css("border-left", src.arrowEdgeSize + "px solid transparent");
				},
				right:function(src, toAppend, toAppendHTML, hasNewPointing) {
					if (hasNewPointing) {
						toAppend.append(toAppendHTML);
					}
					var arrow = $("#" + src.id + "Arrow");
					arrow.css("border-left", src.arrowBaseSize + "px solid " + src.bgColor);
					arrow.css("border-top", src.arrowEdgeSize + "px solid transparent");
					arrow.css("border-bottom", src.arrowEdgeSize + "px solid transparent");
				}
		}
		
		/*
		 * Sets whether the arrow appears above, below, right, or left of the ContentTD if
		 * hasNewPointing is specified and is true.
		 * As adjusts updates the appearance of the arrow.
		 */ 
		this.restructureArrow = function(hasNewPointing) {
			var arrowTD = $("#" + this.id + "ArrowTD");
			var arrowTR = $("#" + this.id + "ArrowTR");
			var contentTR = $("#" + this.id + "ContentTR");
			var arrowOriginal = $("#" + this.id + "Arrow");
			arrowOriginal.attr("style","");
			var arrowHTML = "";
			if (arrowTD.length == 0) {
				arrowHTML = "<td id='" + this.id + "ArrowTD' class='condense'><div id='" + this.id + 
						"Arrow' class='" + this.arrowClass + "'></div></td>";
			} else {
				arrowHTML = "<td id='" + this.id + "ArrowTD' class='condense'>" + arrowTD.html() + "</td>";
			}
			if (hasNewPointing) {
				if (arrowTR.length != 0) {
					arrowTR.remove();
				} else if (arrowTD.length != 0) {
					arrowTD.remove();
				}
			}
			if (! this.pointing in this.POINTING_OPTIONS) {
				this.pointing = "dynamic";
			}
			this.POINTING_OPTIONS[this.pointing](this, contentTR, arrowHTML, hasNewPointing);
		}
		
		this.POSITION_OPTIONS = {
				dynamic:function(src, asso) {
					return this.left;
				},
				top:function(src, asso) {
					src.top = asso.position().top - src.instance.height() - src.distance;
					src.left = asso.position().left + asso.width() / 2.0 - 
							src.instance.width() / 2.0;
				},
				bottom:function(src, asso) {
					src.top = asso.position().top + asso.height() + src.distance;
					src.left = asso.position().left + asso.width() / 2.0 - 
							src.instance.width() / 2.0;
				},
				right:function(src, asso) {
					src.top = asso.position().top + 
							asso.height() / 2.0 - src.instance.height() / 2.0;
					src.left = asso.position().left + asso.width() + src.distance;
				},
				left:function(src, asso) {
					src.top = asso.position().top + 
							asso.height() / 2.0 - src.instance.height() / 2.0;
					src.left = asso.position().left - src.instance.width() - src.distance;
				}
		}
		
		/* Positions the pointy tooltip in the correct spot */
		this.reposition = function() {
			if (this.associate != false) {
				var asso = $("#" + this.associate);
				if (asso.length == 0) {
					this.instance.css("top", this.top + "px");
		    		this.instance.css("left", this.left + "px");
		    		return;
				}
				if (! this.position in this.POSITION_OPTIONS) {
					this.position = "dynamic";
				}
				this.POSITION_OPTIONS[this.position](this, asso);
			} 
    		this.instance.css("top", this.top + "px");
    		this.instance.css("left", this.left + "px");
		}
		
		this.setOptions = function(options) {
			var oldid = this.id;
			var oldassociate = this.associate;
			var oldactive = this.active;
			var oldArrowClass = this.arrowClass;
			var oldContentClass = this.contentClass;
			var oldBgColor = this.bgColor;
			var oldFontColor = this.fontColor;
			var oldCurve = this.curve;
			var oldPointing = this.pointing;
			var oldArrowEdge = this.arrowEdgeSize;
			var oldArrowBase = this.arrowBaseSize;
			var oldOnEvent = this.onEvent;
			var oldOffEvent = this.offEvent;
			$.extend(this, options);
			// Handles if the id changed
			if (oldid != this.id) {
				this.instance.attr("id", this.id);
				if (oldPointing != "none") {
					$("#" + oldid + "Arrow").attr("id", this.id + "Arrow");
					$("#" + oldid + "ArrowTD").attr("id", this.id + "ArrowTD");
				}
				$("#" + oldid + "ContentTD").attr("id", this.id + "ContentTD");
				$("#" + oldid + "ContentTR").attr("id", this.id + "ContentTR");
				PT.instances[oldid] = undefined;
				PT.instances[this.id] = this;
			}
			// If the original associated object does not exist or gets removed,
			// the associate value gets replaced by false
			if (oldassociate != false) {
				if ($('#' + oldassociate).length == 0) {
					oldassociate = false;
				}
			}
			if (this.associate != false) {
				if ($('#' + this.associate).length == 0) {
					this.associate = false;
				}
			}
			// Handle the associated elements and the related effects
			var oldPT = "PT.instances['" + oldid + "']";
			var newPT = "PT.instances['" + this.id + "']";
			var fadeInMethod = ".onEventHandler();";
			var fadeOutMethod = ".offEventHandler();";
			if (this.associate == false && oldassociate != this.associate) {
				// associate was turned off
				$removeAttrPart(oldassociate, oldOnEvent, oldPT + fadeInMethod);
				$removeAttrPart(oldassociate, oldOffEvent, oldPT + fadeOutMethod);
			} else if (oldassociate != this.associate) {
				// associate changed
				if (oldassociate != false) {
					$removeAttrPart(oldassociate, oldOnEvent, oldPT + fadeInMethod);
					$removeAttrPart(oldassociate, oldOffEvent, oldPT + fadeOutMethod);
				}
				$addAttrPart(this.associate, this.onEvent, newPT + fadeInMethod);
				$addAttrPart(this.associate, this.offEvent, newPT + fadeOutMethod);
			} else if ((oldid != this.id && this.associate != false) || 
					oldOnEvent != this.onEvent || oldOffEvent != this.offEvent ) {
				// Case where the ID changed
				$removeAttrPart(this.associate, oldOnEvent, oldPT + fadeInMethod);
				$removeAttrPart(this.associate, oldOffEvent, oldPT + fadeOutMethod);
				$addAttrPart(this.associate, this.onEvent, newPT + fadeInMethod);
				$addAttrPart(this.associate, this.offEvent, newPT + fadeOutMethod);
			}
			
			// Control the color of the PointTip and the appearance of the arrow
			if (oldFontColor != this.fontColor || this.newlyCreated) {
				$("#" + oldid + "ContentTD").css("color", this.fontColor);
			}
			if (oldCurve != this.curve || this.newlyCreated) {
				$("#" + oldid + "ContentTD").css("border-radius", this.curve + "px");
			}
			if (oldBgColor != this.bgColor || this.newlyCreated) {
				$("#" + oldid + "ContentTD").css("background-color", this.bgColor);
				this.restructureArrow(oldPointing != this.pointing);
			} else if (oldArrowEdge != this.arrowEdgeSize || oldArrowBase != this.arrowBaseSize ||
					oldPointing != this.pointing) {
				this.restructureArrow(oldPointing != this.pointing);
			}
			
			// Handles changes in the class of the content
			if (this.contentClass != oldContentClass) {
				$('#' + this.id + "ContentTD").attr("class", this.contentClass);
			}
			if (this.arrowClass != oldArrowClass && this.pointing != "none") {
				$('#' + this.id + "Arrow").attr("class", this.arrowClass);
			}
			
			// Repositions the object no matter what
			this.reposition();
			
			// Handles changes in the visibility and position of the PointTip
			if (!this.showing) {
				if (this.instance.css("opacity") != "0") {
					this.instance.css("opacity", "0");
					this.instance.hide();
					this.available = true;
					this.on = false;
					this.showing = false;
				}
			} else {
				if (this.instance.css("opacity") == "0") {
					this.instance.css("opacity", "");
					this.instance.show();
					this.available = true;
					this.on = true;
					this.showing = true;
				}
			}
			
			this.newlyCreated = false;
			return true;
		}
		
		this.onEventHandler = function() {
			this.associateMouseOn = true;
			if (! this.active) {
				return;
			}
			var src = this;
			setTimeout(function(){
		        if (src.associateMouseOn && src.available) {
		        	src.fadeIn();
		        }
		    }, this.graceperiod);
		}
		
		this.offEventHandler = function() {
			this.associateMouseOn = false;
			if (! this.active) {
				return;
			}
			var src = this;
			setTimeout(function() {
				if (src.on && src.available) {
					src.fadeOut();
				} else if (src.on && !src.available) {
					var src2 = this;
					setTimeout(function() {src2.offEventHandler()}, 50);
				}
			}, this.endperiod);
		}
		
		this.fadeIn = function() {
			this.instance.show();
			this.available = false;
			this.on = true;
			this.showing = true;
			this.reposition();
			var src = this;
			var fadeTime = this.fade;
			if (this.inFade >= 0) {
				fadeTime = this.inFade;
			}
			this.instance.animate({opacity:1}, fadeTime, function() { src.available = true; });
		}
		
		this.fadeOut = function() {
			this.on = false;
			this.available = false;
			this.showing = false;
			var src = this;
			var fadeTime = this.fade;
			if (this.outFade >= 0) {
				fadeTime = this.inFade;
			}
			this.instance.animate({opacity:0}, fadeTime, function() { src.available = true; src.instance.hide(); } );
		}
		
		this.setOptions(options);
		
		/* Deletes this instance */
		this.remove = function() {
			this.instance.remove();
			PT.instances[this.id] = undefined;
			
			if (this.associate != false && $("#" + this.associate).length != 0) {
				$removeAttrPart(this.associate, "onmouseover", "PT.instances['" + this.id + "'].onEventHandler();");
				$removeAttrPart(this.associate, "onmouseout", "PT.instances['" + this.id + "'].offEventHandler();");
			}
			
			for (prop in this) {
				this[prop] = undefined;
			}
			
			this.setOptions = function(options) { return false; };
		}
	}
	
	this.create = function(divid, options) {
		return new PointTip(divid, options);
	}
}

var PT = new PointTipManager();