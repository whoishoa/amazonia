
$(function() {
	positionMenu();
	$("#menuModal").hide();
	if (("#headerCart").length != 0) {
		PT.create("cartTooltipModal",{
				associate:"headerCart",
				bgColor:"#00AAFF",
				pointing: "up",
				position: "bottom",
				distance: 10,
				curve: 5,
				endperiod: 0,
				graceperiod: 500});
		PT.create("helloBubbleModal",{
				associate:"headerTitle",
				bgColor:"#0088EE",
				pointing: "up",
				position: "bottom",
				showing: true,
				active: false,
				distance: -10,
				curve: 5,
				graceperiod: 500});
	}
});

$( window ).resize(function() {
	positionMenu();
	if (("#headercart").length != 0) {
		PT.instances["helloBubbleModal"].reposition();
	}
});

function positionMenu() {
	var asso = $("#headerMenu");
	var menu = $("#menuModal");
	menu.css("top", asso.position().top + asso.height() + 10 + "px");
	menu.css("left", asso.position().left + "px");
}

function toggleMenu() {
	positionMenu();
	$("#menuModal").toggle("fast");
}