<%@ include file="private/Library.jsp"%>

<% title = "Analytics Table"; %>

<% 
	
	int rowSize = 20;
	int columnSize = 10;
	boolean rowNotFound = false;
	boolean columnNotFound = false;;
	int maxCharsPerProduct = 10; //product names display only the first 10 characters.
	//control variables for queries:
	String rowType = request.getParameter("rowType"); //either 'states' or 'categories'
	
	if (rowType == null) {
	   response.sendRedirect("analytics?redirect=true");
	} else {
	
	//String ageFilter = request.getParameter("ageFilter"); //minimum age in the age filter.
	String stateFilter = request.getParameter("stateFilter"); //name of state being filtered.
	String categoryFilter = request.getParameter("categoryFilter"); //this is the id of the category being filtered.
	String categoryFilterName = "All Categories";
	if ( ! categoryFilter.equals("all") ) {
		try {
	    categoryFilterName = categoryFilter.substring( categoryFilter.indexOf("|") + 1 );
		categoryFilter = categoryFilter.substring( 0, categoryFilter.indexOf("|") );
		} catch (Exception e) {
			categoryFilterName = "All Categories";
			categoryFilter = "all";
		}
	}

	String rowOffset = request.getParameter("rowOffset"); //the ordered # of the first element displayed on the rows
	String columnOffset = request.getParameter("columnOffset"); //the ordered # of the first element displayed on the columns
	boolean allStatesChosen = false;
	boolean allCategoriesChosen = false;

	String result; //used much later for displaying data at each table index.

	//sizes for displayed table:
	int rowCount = 0;
	int columnCount = 0;

	//figure out the stateFilter:
	if(stateFilter.equals("all")) { allStatesChosen = true; }

	String sqlStateFilter = "";
	String sqlUserStateFilter = "";
	if(!allStatesChosen) { //add the state filter
		sqlStateFilter = " AND u.state = '" + stateFilter +"'";
		sqlUserStateFilter = " AND s.name = '"+stateFilter+"'";
	}

	//figure out categories filter:
	String sqlCategoryFilter = "";
	String defaultRowTable = "pre" + rowType;
	if(categoryFilter.equals("all")) { 
	    allCategoriesChosen = true; 
	} else { 
		allCategoriesChosen = false;
		defaultRowTable += "Categories";
		sqlCategoryFilter = " AND p.cid = "+categoryFilter;
	}
	//do a query to get the rows:
	String rowDrop = "DROP TABLE IF EXISTS temprows";
	String rowCreate = "CREATE TEMP TABLE temprows AS";
	String rowQuery = "SELECT * FROM temprows";
	
	if(rowType.equals("users")) { //do a state query
		rowCreate += " SELECT u.name AS rowname, u.id AS rowid, sum AS rowsum FROM "
			+ defaultRowTable
			+ ", users AS u"
			+ " WHERE uid = u.id"
			+ sqlCategoryFilter.replace("p.cid", "cid")
			+ sqlStateFilter
			+ " ORDER BY rowsum DESC, rowid LIMIT 20";
	} else {
		rowCreate += " SELECT s.name AS rowname, s.id AS rowid, sum AS rowsum FROM "
			+ defaultRowTable
			+ ", states AS s"
			+ " WHERE sid = s.id"
			+ sqlCategoryFilter.replace("p.cid", "cid")
			+ sqlUserStateFilter
			+ " ORDER BY rowsum DESC, rowid LIMIT 20";
	}

	//do a query to get the rows:
	String productDrop = "DROP TABLE IF EXISTS tempcols";
	String productCreate = "CREATE TEMP TABLE tempcols AS";
	String productQuery = " SELECT * FROM tempcols";
	
	if ( allStatesChosen ) {
		productCreate += " SELECT p.name AS colname, p.id AS colid, sum AS colsum"
			+ " FROM preProducts, products AS p"
			+ " WHERE pid = p.id"
			+ sqlCategoryFilter
			+ " ORDER BY colsum DESC, colid LIMIT 10";
	} else {
		productCreate += " SELECT p.name AS colname, p.id AS colid, sum AS colsum"
			+ " FROM preStatesProducts, products AS p, states AS s"
			+ " WHERE pid = p.id"
			+ " AND sid = s.id"
			+ sqlCategoryFilter
			+ sqlUserStateFilter
			+ " ORDER BY colsum DESC, colid LIMIT 10";
	}
	
	long startTime = System.nanoTime();
		pstmt = conn.prepareStatement(rowDrop);
		pstmt.execute();
		pstmt = conn.prepareStatement(rowCreate);
		pstmt.execute();
		pstmt = conn.prepareStatement(rowQuery);
		rs = pstmt.executeQuery();
	long endTime = System.nanoTime();
	long rowTime = endTime - startTime;
	
	
	startTime = System.nanoTime();
		pstmt = conn.prepareStatement(productDrop);
		pstmt.execute();
		pstmt = conn.prepareStatement(productCreate);
		pstmt.execute();
		pstmt = conn.prepareStatement(productQuery); //columns(products)
		rs2 = pstmt.executeQuery();
	endTime = System.nanoTime();
	long productsTime = endTime - startTime;
	
	ArrayList<String> colHeader = new ArrayList<String>();
	
	String firstProduct = null;
	String lastProduct = null;
	int firstProductID = -1;
	int lastProductID = -1;
	while (rs2.next()) {
	    String productName = rs2.getString("colname"); //grab the full product name. Cut out the first 10 in the next line of code.
		if (firstProduct == null) {
			firstProductID = rs2.getInt("colid");
		    firstProduct = productName;
		}
		lastProductID = rs2.getInt("colid");
		lastProduct = productName;
		if(productName.length() > maxCharsPerProduct) {
			productName = productName.substring(0, maxCharsPerProduct); //grab the (10) first characters only.
		}
	    colHeader.add(productName + " (" + rs2.getInt("colsum") + ")");
		
	}
	
	ArrayList<String> rowHeader = new ArrayList<String>();
	String arrRowName;
	int arrRowSum;
	String firstUserName = null;
	String lastUserName = null;
	while(rs.next()) {
		arrRowName = rs.getString("rowname"); //grab the row name.
			if(firstUserName == null) { firstUserName = arrRowName; }
			lastUserName = arrRowName;
		arrRowSum = rs.getInt("rowsum"); //store sum
		rowHeader.add(arrRowName + " (" + arrRowSum + ")");
	}
	
	//check for row/column not being found.
	if(rowHeader.size() == 0) { rowNotFound = true; }
	if(colHeader.size() == 0) { columnNotFound = true; }
	int colOffsetInt = 0;
	
	try {
	   colOffsetInt = Integer.parseInt(columnOffset);
	} catch (NumberFormatException e) {
	   
	}
	
	int rowOffsetInt = 0;
	try {
	   rowOffsetInt = Integer.parseInt(rowOffset);
	} catch (NumberFormatException e) {
	   
	}
	
	String innerQuery;
	String innerTable = "preusersproducts";
	String innerFirstWhere = " WHERE sa.uid = rowid";
	if (rowType.equals("states")) {
	    innerTable = "prestatesproducts";
		innerFirstWhere = " WHERE sa.sid = rowid";
	} 
	innerQuery = "select sa.sum AS cellval"
			+ " FROM " + innerTable + " as SA, temprows, tempcols"
			+ innerFirstWhere
			+ " AND sa.pid = colid"
			+ " ORDER BY rowsum DESC, rowid, colsum DESC, colid";
		
	pstmt = conn.prepareStatement(innerQuery); //inner query (product+user's sales sum)
	
	startTime = System.nanoTime();
	rs3 = pstmt.executeQuery();
	endTime = System.nanoTime();
	long innerTime = endTime - startTime;

%>

<%@ include file="private/header.jsp" %>
<%@ include file="private/middle.jsp" %>

<h1>Sales Report</h1>
<br>

<%
if ( Integer.parseInt(rowOffset) == 0 &&  Integer.parseInt(columnOffset) == 0 ) {
%>
   <div class="noteDialog">
   <h3>Selected Filter Options:</h3>
   <b>Row Type:</b> <%= (rowType.equals("users"))?"Customers":"States" %><br>
   <b>State Filter:</b> <%= (stateFilter.equals("all"))?"All States":stateFilter %><br>
   <b>Category:</b> <%= categoryFilterName %>
   </div>
<%
}
%>

<!-- Display the table of results -->
 <% 
 //print error for no more/found row
 if(rowNotFound) { %>
	<div class="errorDialog">No <%=rowType%> were found.</div>
 <% } %>
 
 <%//print error for no more/found columns
 if(columnNotFound) { %>
	 <div class="errorDialog">No products were found</div>
 <% } %>
 
 <%//display table for both columns and rows being found!
 if(!columnNotFound && !rowNotFound) { %>
<table class="analysisChart" border=1 cellpadding=5 align="center">
    <TR>

        <TD></TD>
		<% for(int j = 0; j < colHeader.size(); j++) { %>
					<TH><%= colHeader.get(j) %></TH>
		<% } //end for%>

    </TR>
	<%int cellSum = 0;%>
    <% for(int i = 0; i < rowHeader.size(); i++) { //start index at 1 because of sql indexing. Loop over rows. %>
			<TR>
				<TH><%=rowHeader.get(i)%></TH>
				<% for(int j = 1; j <= colHeader.size(); j++){//index over inner columns(rs3) {
					rs3.next();
					//PRINT INNER QUERY HERE!!
					cellSum = rs3.getInt("cellval"); %>
					<TD><%=cellSum%></TD>
				<% } //end for(j) %>
			</TR>	
    <% } //end for(i) %>
</table>

<br/>

<br>
<div class="noteDialog">
This report took the following amounts of time to be generated:
</div>
<table class="timesTable">
 <tr>
  <td>
   Rows:
  </td>
  <td>
   <%= rowTime / 1000000.0 %> ms
  </td>
 </tr>
 <tr>
  <td>
   Columns:
  </td>
  <td>
   <%= productsTime / 1000000.0 %> ms
  </td>
 </tr>
 <tr>
  <td>
   Table:
  </td>
  <td>
   <%= innerTime / 1000000.0 %> ms
  </td>
 </tr>
 <tr>
  <td>
   Total:
  </td>
  <td>
   <%= (rowTime + productsTime + innerTime) / 1000000.0 %> ms
  </td>
 </tr>
</tr>
</table>

<% } } %>
<%@ include file="private/ender.jsp" %>
