<%@ include file="private/Library.jsp"%>

<% title = "Sales Analytics"; %>

<%@ include file="private/header.jsp" %>
<%@ include file="private/middle.jsp" %>
   
    <% if (request.getParameter("redirect") != null) { %>
   <div class="errorDialog">Please run a query first to see the report</div>
   <br>
   <% } %>
   
    <h1>Sales Analytics</h1>   
   
    <br />

    <form method="GET" action="analysis">

	    <table class="analyticsTable">
		  <tr>
		    <td>
        Select Row Type:
		    </td>
			<td>
        <!-- AGE CHOICES -->
        <select class="registerSelect" name="rowType">
            <option value="users">Customers</option>
            <option value="states">States</option>
        </select>
		    </td>
	       </tr>
		   
			<tr>
              <td>
        Select State Filter:
		      </td>
			  <td>
        <!-- STATE CHOICES -->
        <select class="registerSelect" name="stateFilter">
                <option value="all">All States</option>
                <option value='Alabama'>Alabama</option>
                <option value='Alaska'>Alaska</option>
                <option value='Arizona'>Arizona</option>
                <option value='Arkansas'>Arkansas</option>
                <option value='California'>California</option>
                <option value='Colorado'>Colorado</option>
                <option value='Connecticut'>Connecticut</option>
                <option value='Delaware'>Delaware</option>
                <option value='Florida'>Florida</option>
                <option value='Georgia'>Georgia</option>
                <option value='Hawaii'>Hawaii</option>
                <option value='Idaho'>Idaho</option>
                <option value='Illinois'>Illinois</option>
                <option value='Indiana'>Indiana</option>
                <option value='Iowa'>Iowa</option>
                <option value='Kansas'>Kansas</option>
                <option value='Kentucky'>Kentucky</option>
                <option value='Louisiana'>Louisiana</option>
                <option value='Maine'>Maine</option>
                <option value='Maryland'>Maryland</option>
                <option value='Massachusetts'>Massachusetts</option>
                <option value='Michigan'>Michigan</option>
                <option value='Minnesota'>Minnesota</option>
                <option value='Mississippi'>Mississippi</option>
                <option value='Missouri'>Missouri</option>
                <option value='Montana'>Montana</option>
                <option value='Nebraska'>Nebraska</option>
                <option value='Nevada'>Nevada</option>
                <option value='New Hampshire'>New Hampshire</option>
                <option value='New Jersey'>New Jersey</option>
                <option value='New Mexico'>New Mexico</option>
                <option value='New York'>New York</option>
                <option value='North Carolina'>North Carolina</option>
                <option value='North Dakota'>North Dakota</option>
                <option value='Ohio'>Ohio</option>
                <option value='Oklahoma'>Oklahoma</option>
                <option value='Oregon'>Oregon</option>
                <option value='Pennsylvania'>Pennsylvania</option>
                <option value='Rhode Island'>Rhode Island</option>
                <option value='South Carolina'>South Carolina</option>
                <option value='South Dakota'>South Dakota</option>
                <option value='Tennessee'>Tennessee</option>
                <option value='Texas'>Texas</option>
                <option value='Utah'>Utah</option>
                <option value='Vermont'>Vermont</option>
                <option value='Virginia'>Virginia</option>
                <option value='Washington'>Washington</option>
                <option value='West Virginia'>West Virginia</option>
                <option value='Wisconsin'>Wisconsin</option>
                <option value='Wyoming'>Wyoming</option>
            </select>
			   </td>
			 </tr>
			 <tr>
			   <td>
    Select a category:
	           </td>
			   <td>
    <select class="registerSelect" name="categoryFilter">
        <option value="all">All Categories</option>
        <%

            pstmt = conn.prepareStatement(" SELECT DISTINCT name, id "
										 +" FROM categories"
										 +" ORDER BY name");
            rs = pstmt.executeQuery();
            while (rs.next()) {
				String categoryName = rs.getString("name");
                int categoryFilter = rs.getInt("id");
        %>
                <option value="<%=categoryFilter%>|<%=categoryName%>"><%=categoryName%></option>

            <% } //end while %> 

    </select>
	           </td>
			 </tr>
		  </table>
    <br/>
    <input type="hidden" name="rowOffset" value="0">
    <input type="hidden" name="columnOffset" value="0">
<!--	<input type="hidden" name="ageFilter" value="all"> -->
	<br>
    <input type="submit" class="mainButton" name="submit" value="Run Query">
    </form>
<%@ include file="private/ender.jsp" %>