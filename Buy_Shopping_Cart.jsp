<%@ include file="private/Library.jsp"%>

<% title = "Buy Shopping Cart"; %>
<%@ include file="private/header.jsp" %>
<%@ include file="private/middle.jsp" %>
<%if (session.getAttribute("role") != null && session.getAttribute("role").equals("customer")) {%>
	
        <br />
<% 
	pstmt = conn.prepareStatement("SELECT * FROM carts WHERE uid="+session.getAttribute("user_id"));
	rs = pstmt.executeQuery();
	
	if (rs.next()) {
%>
        <h3>Your Shopping Cart</h3>
        <table border=1 cellpadding=5 align="center">
          <tr>
            <th>Product Name</th>
            <th>SKU</th>
            <th>Quantity</th>
                        <th>Price</th>
                        <th>Amount Price</th>
          </tr>
                                <%//get user's cart
                                pstmt = conn.prepareStatement("SELECT p.name, p.SKU, c.quantity, p.price "
                                                                                                +"FROM products AS p,carts AS c "
                                                                                                +"WHERE c.uid = ? "
                                                                                                +"AND c.pid = p.ID");

                                int user_id = (Integer) session.getAttribute("user_id");
                                pstmt.setInt(1, user_id);
                                rs = pstmt.executeQuery();
                                double sum=0; //for grand total
                                while(rs.next()) {
                                %>
                                <tr>
                                        <td><%=rs.getString("name")%></td>
                                        <td><%=rs.getString("SKU")%></td>
                                        <td><%=rs.getInt("quantity")%></td>
                                        <td>$<%=formatPrice(rs.getInt("price"))%></td>
                                        <%double quantityAppliedPrice = rs.getInt("quantity") * rs.getInt("price");
                                          sum += quantityAppliedPrice;%>
                                        <td>$<%=formatPrice(quantityAppliedPrice)%></td>
                                </tr>
                                <% } %>
                                </table>
                                <br />
                                Grand Total = $<%=formatPrice(sum)%>

                <%if (sum > 0) { //if we had items in cart %>
                <br /><br><br>
                <form method="POST" action="confirmation">
                        Enter Credit Card Number:
                        <br />
                        <input type="text" size="16" />
                        <input type="submit" name="registerButton" value="Purchase" />
                </form>
                <% } //endif %>
                
            <% } else { %>
            <div class="errorDialog">
            	Your cart is currently empty.
            </div>
            <%} %>
      <% } else { /* The user is not logged in or not a customer */ %>

<div class="errorDialog">
	You must be logged in as a customer to view this page!
</div>

<% } %>
				
<%@ include file="private/ender.jsp" %>
