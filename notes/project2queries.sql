States RowQuery
=================

SELECT s.name, 
    s.id, 
    SUM( 
        CASE WHEN p.cid = 7 THEN 
            sa.price * sa.quantity 
        ELSE 
            0 
        END
    ) AS sumsales
FROM (
    SELECT * FROM states 
    WHERE states.name = 'California' 
    ORDER BY states.name
    LIMIT rowSize OFFSET rowOffset;
) AS s 
    LEFT JOIN users AS u 
        ON s.name = u.state 
        AND u.age BETWEEN 19 AND 45
    LEFT JOIN sales AS sa 
        ON sa.uid = u.id
    LEFT JOIN products AS p 
        ON p.id = sa.pid
GROUP BY s.name, 
    s.id
ORDER BY s.name


====================
Customer RowQuery
====================

SELECT u.name, 
    u.id, 
    SUM( 
        CASE WHEN p.cid = 7 THEN 
            sa.price * sa.quantity 
        ELSE 
            0 
        END
    ) AS sumsales
FROM (
    SELECT * FROM users 
    WHERE 1 = 1
        AND u.age BETWEEN 19 AND 45
        AND u.state = 'California'
    ORDER BY users.name 
    LIMIT 20 OFFSET 0
) AS u
    LEFT JOIN states AS s 
        ON s.name = u.state
    LEFT JOIN sales AS sa 
        ON sa.uid = u.id
    LEFT JOIN products AS p 
        ON p.id = sa.pid
GROUP BY u.id,
    u.name
ORDER BY u.name


=================
ProductsQuery
=================

SELECT p.name,
    p.id,
    SUM( 
        CASE WHEN 1 = 1
                AND u.state = 'California'
                AND u.age BETWEEN 19 AND 45 THEN
            sa.price * sa.quantity
        ELSE
            0
        END
    ) AS sumsales
FROM (
    SELECT * FROM products 
    WHERE 1 = 1 
        AND p.cid = 7
    ORDER BY p.name,
        p.id
    LIMIT 10 OFFSET 0
) AS p
    LEFT JOIN sales AS sa 
        ON sa.pid = p.id
    LEFT JOIN users AS u
        ON sa.uid = u.id
GROUP BY p.id,
    p.name
ORDER BY p.name,
    p.id

======================
Customer InnerQuery
======================

SELECT u.name, 
    p.name, 
    SUM(
        CASE WHEN sales.uid = u.id THEN 
            sales.quantity * sales.price 
        ELSE 
            0 
        END
    ) AS sumSales
FROM (
    SELECT * FROM users
    WHERE 1 = 1
        AND users.age BETWEEN 19 AND 45
        AND users.state = 'California'
        ORDER BY users.name
        LIMIT 20 OFFSET 0
) AS u,
    (
        SELECT * FROM products
        WHERE 1 = 1
            AND products.cid = 7
        ORDER BY products.name
        LIMIT 10 OFFSET 0
    ) AS p 
        LEFT JOIN sales 
            ON sales.pid = p.id
GROUP by u.name,
    p.name
ORDER BY u.name,
    p.name


==================
States InnerQuery
==================

SELECT s.name, 
    p.name, 
    SUM(
        CASE WHEN s.name = u.state
                AND u.age BETWEEN 19 AND 45 THEN
            sales.price * sales.quantity 
        ElSE 
            0 
        END
    ) AS sumsales
FROM (
    SELECT * FROM states
    WHERE state.name = 'California'
    ORDER BY states.name
    LIMIT 20 OFFSET 0
) AS s, 
    (
        SELECT * FROM products
    	WHERE products.cid = 7
    	ORDER BY products.name
    	LIMIT 10 OFFSET 0
    ) AS p
        LEFT JOIN sales 
            ON sales.pid = p.id
        LEFT JOIN users AS u 
            ON sales.uid = u.id
GROUP BY s.name,
    p.name
ORDER BY s.name,
    p.name

