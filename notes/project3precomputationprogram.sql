--THIS PAGE BOTH CREATES AND INSERTS INTO ALL PRECOMPUTED TABLES

------------------------------- Creating Users Products
-------------------------------------------------------------------------------
select up.uid, up.pid, (CASE WHEN sa.sum IS NULL THEN 0 ELSE sa.sum END ) AS sum 
INTO preUsersProducts
FROM (
    SELECT users.id AS uid, products.id AS pid
    FROM users, products
) AS up
    LEFT JOIN ( -- Does all of the multiplication
    	SELECT sa.uid, sa.pid, SUM(sa.price * sa.quantity) AS sum 
		FROM sales AS sa
		GROUP BY sa.uid, sa.pid
    ) AS sa
        ON sa.pid = up.pid and sa.uid = up.uid
ORDER BY uid, pid DESC;

create index pup_uid_index ON preUsersProducts (uid); 	--Then start making index to speed up table
create index pup_pid_index ON preUsersProducts (pid); --Then start making index to speed up table
------------------------------- Creating States Products
-------------------------------------------------------------------------------
SELECT 
    allsp.sid, 
    allsp.pid, 
    CASE WHEN somesp.sum IS NULL THEN 0 ELSE somesp.sum END 
INTO preStatesProducts
FROM (
    SELECT 
        states.id AS sid, 
        products.id AS pid
    FROM states, products
) AS allsp 
    LEFT JOIN (
    
        SELECT s.id AS sid, pid, SUM(sum) as sum
FROM preUsersProducts, users AS u, states AS s
WHERE uid = u.id 
    and s.name = u.state
GROUP BY s.id, pid

    ) AS somesp
        ON allsp.sid = somesp.sid and allsp.pid = somesp.pid
ORDER BY sum DESC;
------------------------------- Creating Users Categories
-------------------------------------------------------------------------------
SELECT 
    alluc.uid, 
    alluc.cid, 
    CASE WHEN someuc.sum IS NULL THEN 0 ELSE someuc.sum END 
INTO preUsersCategories
FROM (
    SELECT 
        users.id AS uid, 
        categories.id AS cid
    FROM users, categories
) AS alluc 
    LEFT JOIN (
        SELECT 
            uid, 
            p.cid AS cid, 
            SUM(sum) as sum
        FROM preUsersProducts, products AS p
        WHERE pid = p.id
        GROUP BY p.cid, uid
        ORDER BY sum DESC
    ) AS someuc
        ON alluc.uid = someuc.uid and alluc.cid = someuc.cid
ORDER BY sum DESC;

------------------------------- Creating States Categories
-------------------------------------------------------------------------------
SELECT 
    allsc.sid, 
    allsc.cid, 
    CASE WHEN somesc.sum IS NULL THEN 0 ELSE somesc.sum END 
INTO preStatesCategories
FROM (
    SELECT 
        states.id AS sid, 
        categories.id AS cid
    FROM states, categories
) AS allsc 
    LEFT JOIN (
        SELECT 
            sid, 
            p.cid AS cid, 
            SUM(sum) AS sum
        FROM preStatesProducts, products AS p
        WHERE pid = p.id
        GROUP BY p.cid, sid
    ) AS somesc
        ON allsc.sid = somesc.sid AND allsc.cid = somesc.cid
ORDER BY sum DESC;

------------------------------- Creating Users
-------------------------------------------------------------------------------
SELECT uid, SUM(sum) AS sum
INTO preUsers
FROM preUsersCategories
GROUP BY uid
ORDER BY sum DESC;

------------------------------- Creating Products
-------------------------------------------------------------------------------
SELECT pid, SUM(sum) AS sum
INTO preProducts
FROM preStatesProducts
GROUP BY pid
ORDER BY sum DESC;

------------------------------- Creating States
-------------------------------------------------------------------------------
SELECT sid, SUM(sum) AS sum 
INTO preStates
FROM prestatescategories 
GROUP BY sid
ORDER BY sum desc;
