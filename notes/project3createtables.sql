We did not use CREATE TABLE to create our precomputed tables.

Please refer to project3precomputationprogram.sql to see how the precomputed tables were made.

The professor said it was okay if we did not include a CREATE TABLE file in our final submission
on https://csemoodle.ucsd.edu/mod/forum/discuss.php?d=27623